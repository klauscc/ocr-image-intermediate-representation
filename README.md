# 1. Code Structure

```
|-- ocr-data-synthesis      generate dataset
|-- text_recon              training and testing
|-- tfcx                    my tensorflow toolbox
```

See the `README.md` inside each folder to see usage.
