# -*- coding: utf-8 -*-
#================================================================
#   God Bless You.
#
#   author: klaus
#   email: chengfeng2333@gmail.com
#   created date: 2019/12/08
#   description:
#
#================================================================

import numpy as np
import tensorflow as tf

PAD = "[PAD]"
PAD_ID = 0
EOS = "[EOS]"
EOS_ID = 1
UNKNOWN = "`"
UNKNOWN_ID = 2
RESERVED_TOKENS = [PAD, EOS, UNKNOWN]


def read_vocab_file(vocab_file):
    """read vocabulary from vocab_file. The RESERVED_TOKENS will be insert to the begining
    of the list.

    Args:
        vocab_file: String. File path to save the vocabularies.

    Returns: List of the vocabularies.

    """
    return np.array(RESERVED_TOKENS + open(vocab_file, "r").read().splitlines())


class Tokenizer(object):
    """vocab-id tokenizer and untokenizer"""

    def __init__(self, vocab_file):
        super(Tokenizer, self).__init__()

        # Lazy initialize the lookup tables as they should be created
        # in the same graph.
        self.vocab_to_id_table = None
        self.id_to_vocab_table = None

        self.vocab = read_vocab_file(vocab_file)
        self.idxs = np.arange(len(self.vocab))

    def initialize(self):
        """manually create the vocab tables in a Graph.

        """
        self.vocab_to_id_table = tf.lookup.StaticHashTable(tf.lookup.KeyValueTensorInitializer(
            self.vocab, self.idxs, key_dtype=tf.string, value_dtype=tf.int64),
                                                           default_value=UNKNOWN_ID)
        self.id_to_vocab_table = tf.lookup.StaticHashTable(tf.lookup.KeyValueTensorInitializer(
            self.idxs, self.vocab, key_dtype=tf.int64, value_dtype=tf.string),
                                                           default_value=UNKNOWN)

    def tokenize(self, text):
        """tokenize text. Convert text to ids.

        Args:
            text: Keys to look up. May be either a SparseTensor or dense Tensor.

        Returns: 
            A SparseTensor if keys are sparse, otherwise a dense Tensor.
            The corresponding vocabs are replaced by the ids.

        """
        if self.vocab_to_id_table is None:
            raise ValueError("vocab to id lookup table should be initialized first")
        return self.vocab_to_id_table.lookup(text)

    def detokenize(self, ids):
        """detokenize ids. Convert ids back to text.

        Args:
            ids: Keys to look up. May be either a SparseTensor or dense Tensor.

        Returns:
            A SparseTensor if keys are sparse, otherwise a dense Tensor.
            The corresponding ids are replaced by the vocabs.

        """
        if self.id_to_vocab_table is None:
            raise ValueError("id to vocab lookup table should be initialized first")
        return self.id_to_vocab_table.lookup(ids)
