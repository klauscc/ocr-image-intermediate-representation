# -*- coding: utf-8 -*-
#================================================================
#   God Bless You.
#
#   author: klaus
#   email: chengfeng2333@gmail.com
#   created date: 2019/09/26
#   description:
#
#================================================================

import os
import time
import tensorflow as tf
import tensorflow.keras as keras
from easydict import EasyDict

from .callbacks.ckpt_callbacks import CheckpointCallback


class BasicTrainer(object):
    """Basic Trainer. The keras model is good enough."""

    def __init__(self, params, net=None):
        super(BasicTrainer, self).__init__()
        self.params = params
        self.net = net

        self.print = self.params.get("print_fn", print)

        self.initiate()

        # if params.get("debug", False):
            # self.train_step = tf.function(self.train_step)
            # self.test_step = tf.function(self.test_step)

    def initiate(self):
        #initiate metrics
        self._metrics = EasyDict()
        self._train_metrics = EasyDict()
        self._val_metrics = EasyDict()

        # create loss for both train and val
        self.train_loss = tf.keras.metrics.Mean(name="train_loss")
        self.val_loss = tf.keras.metrics.Mean(name="val_loss")
        self.metric("loss", train_metric=self.train_loss, val_metric=self.val_loss)

        #initiate logs
        self.logs = EasyDict()
        self.update_logs()

        #initiate optimizer
        # self.optimizer = self.get_optimizer()
        # checkpoint
        self.define_ckpt()
        #callbacks
        self.callbacks = self.define_default_callbacks()

        #tf summary
        self.create_summary_writer()

    def create_summary_writer(self):
        log_dir = os.path.join(self.params.workspace, "tensorboard")
        os.makedirs(log_dir, exist_ok=True)
        self.train_summary_writer = tf.summary.create_file_writer(os.path.join(log_dir, "train"))
        self.val_summary_writer = tf.summary.create_file_writer(os.path.join(log_dir, "val"))

    def loss_object(self, inputs, predictions):
        """calculate the loss between predictions and targets.

        Args:
            inputs: Dict. The output of the dataset. Both the model inputs 
                    and targets are contained in the dict.
            predictions: Dict or tuple or Tensor. The output of the model.

        Returns: Tensor. The loss.

        """
        pass

    def train_step(self, inp, tar, step):
        """train step.

        Args:
            inp: model input.
            tar: model target.
            step: current step.

        """
        pass

    def test_step(self, inp, tar, step):
        """test step

        Args:
            inp: model input.
            tar: model target.
            step: current step.

        """
        pass

    def train_metric(self, key, metric=None):
        """add or return train metric.

        Args:
            key: String. The actual key will be `"train_"+key`.
        Kwargs:
            metric: Metric. If the `key` is a new metric, value must be specified.

        Returns: Metric.

        """
        actual_key = "train_" + key
        return self.metric(key, train_metric=metric, mode="train")

    def val_metric(self, key, metric=None):
        """add or return val metric.

        Args:
            key: String. The actual key will be `"val_"+key`.
        Kwargs:
            metric: Metric. If the `key` is a new metric, value must be specified.

        Returns: Metric.

        """
        actual_key = "val_" + key
        return self.metric(key, val_metric=metric, mode="val")

    def metric(self, key, train_metric=None, val_metric=None, mode="both"):
        """add or return metric.
        Args:
            key: String. The metric name.

        Kwargs:
            train_metric: Metric. Should be specified if the metric has not been created.
            val_metric: Metric. Should be specified if mode is "both".
            mode: String. Either "train" or "val" or "both". If mode is "both", 
                the train metric will be returned.

        Returns: the metric.

        """
        train_key = "train_" + key
        val_key = "val_" + key

        if mode == "both":
            assert train_metric is not None and val_metric is not None, \
                    "Both train_metric and val_metric should not be None when mode is {}".format(mode)
            self._metrics[train_key] = train_metric
            self._train_metrics[train_key] = train_metric
            self._metrics[val_key] = val_metric
            self._val_metrics[val_key] = val_metric
            return train_metric
        elif mode == "train":
            if train_metric is not None:
                self._metrics[train_key] = train_metric
                self._train_metrics[train_key] = train_metric
            else:
                assert train_key in self._metrics, "Metric: {} is not created".format(train_key)

            return self._metrics[train_key]
        else:
            if val_metric is not None:
                self._metrics[val_key] = val_metric
                self._val_metrics[val_key] = val_metric
            else:
                assert val_key in self._metrics, "Metric: {} is not created".format(val_key)
            return self._metrics[val_key]

    def add_callbacks(self, callbacks):
        """add additional callbacks.

        Args:
            callbacks: List. Each element is an tf.keras.callbacks.Callback.

        Returns: None

        """
        for callback in callbacks:
            callback.set_model(self.net)
        self.callbacks += callbacks

    def define_ckpt(self):
        self.ckpt = tf.train.Checkpoint(step=tf.Variable(0),
                                        optimizer=self.net.optimizer,
                                        model=self.net)
        return self.ckpt

    def define_default_callbacks(self):
        """define default callbacks: ModelSaveCallbacks
        Returns: TODO

        """
        callbacks = []
        params = self.params
        ckpt_path = os.path.join(params.workspace, "ckpts")

        #checkpoint callback
        callbacks.append(
            CheckpointCallback(ckpt_path,
                               ckpt=self.ckpt,
                               save_freq=params.get("save_freq", 1),
                               max_to_keep=params.get("max_to_keep", 100)))
        return callbacks

    def update_logs(self):
        for k, v in self._metrics.items():
            self.logs[k] = v.result()

    def get_optimizer(self):
        """TODO: Docstring for get_optimizer.
        Returns: TODO

        """
        params = self.params
        other_args = {}
        if "clipnorm" in params:
            other_args["clipnorm"] = params.clipnorm
        if "clipvalue" in params:
            other_args["clipvalue"] = params.clipvalue

        if params.lr_decay_policy == "exp":
            lr = tf.keras.optimizers.schedules.ExponentialDecay(params.init_lr,
                                                                params.decay_steps,
                                                                params.decay_rate,
                                                                staircase=params.staircase)
        else:
            lr = params.init_lr
        if params.optimizer == "adam":
            beta_1 = params.get("beta_1", 0.9)
            beta_2 = params.get("beta_2", 0.999)
            optimizer = tf.keras.optimizers.Adam(lr, beta_1=beta_1, beta_2=beta_2, **other_args)
        elif params.optimizer == "sgd":
            momentum = params.get("momentum", 0.9)
            optimizer = tf.keras.optimizers.SGD(lr, momentum=momentum, **other_args)
        else:
            raise ValueError("unsupported optimizer: {}".format(params.optimizer))
        return optimizer

    def summary_net(self, net):
        if hasattr(net, "summary"):
            net.summary(print_fn=self.print)
            for layer in net.layers:
                if hasattr(layer, "summary"):
                    self.summary_net(layer)

    def print_metrics(self, metric_dict):
        res = ""
        for k, v in metric_dict.items():
            res += "{}:{:.7f},".format(k, v.result())
        return res

    def _get_size_from_inputs(self, inputs):
        """ return the batch size from inputs.
        """
        if isinstance(inputs, (list, tuple)):
            return inputs[0].shape[0]
        elif isinstance(inputs, dict):
            return inputs[list(inputs.keys())[0]].shape[0]
        else:
            raise ValueError("inputs is not in (list, tuple, dict), but :{}".format(type(inputs)))

    def train(self, datasets, test_datasets=None, test_step=None, epochs=400, test_period=1):
        """TODO: Docstring for train.
        Returns: TODO

        """

        begin_time = time.time()
        fmt = "Epoch {}, step {}, cost {:.4f}s. metrics: {}"
        callbacks = self.callbacks
        [metric.reset_states() for k, metric in self._metrics.items()]    #reset metrics
        [callback.on_train_begin() for callback in callbacks]
        cur_step = 0
        for epoch in range(epochs):
            #train
            with self.train_summary_writer.as_default():
                [callback.on_epoch_begin(epoch, self.logs) for callback in callbacks]
                # self.print("epoch {}. learning rate: {}".format(
                # epoch,
                # self.net.optimizer._decayed_lr(tf.float32).numpy()))
                epoch_t1 = time.time()
                for step, (inp, tar) in enumerate(datasets):

                    t1 = time.time()

                    logs = self.logs.copy()
                    logs["size"] = self._get_size_from_inputs(inp)
                    logs["batch"] = step
                    [callback.on_train_batch_begin(step, logs) for callback in callbacks]

                    self.train_step(inp, tar, cur_step)
                    self.update_logs()

                    logs = self.logs.copy()
                    logs["size"] = self._get_size_from_inputs(inp)
                    logs["batch"] = step
                    [callback.on_train_batch_end(step, logs) for callback in callbacks]

                    # if epoch == 0 and step == 0:
                    # self.summary_net(self.net)
                    t2 = time.time()
                    self.print(
                        fmt.format(epoch, step, t2 - t1, self.print_metrics(self._train_metrics)))

                    cur_step += 1

            #test
            with self.val_summary_writer.as_default():
                if test_datasets and (epoch + 1) % test_period == 0:
                    self.print("begin evaluation. epoch {}".format(epoch))
                    [callback.on_test_begin(self.logs) for callback in callbacks]
                    for step, (inp, tar) in enumerate(test_datasets):
                        if test_step and step == test_step:
                            break
                        t1 = time.time()

                        logs = self.logs.copy()
                        logs["size"] = self._get_size_from_inputs(inp)
                        logs["batch"] = step
                        [callback.on_test_batch_begin(step, logs) for callback in callbacks]

                        result = self.test_step(inp, tar, step)
                        self.update_logs()

                        logs = self.logs.copy()
                        logs["size"] = self._get_size_from_inputs(inp)
                        logs["batch"] = step
                        logs["result"] = result
                        [callback.on_test_batch_end(step, logs) for callback in callbacks]

                        t2 = time.time()
                        self.print(
                            fmt.format(epoch, step, t2 - t1, self.print_metrics(self._val_metrics)))
                    [callback.on_test_end(self.logs) for callback in callbacks]

            [callback.on_epoch_end(epoch, self.logs) for callback in callbacks]
            for k, metric in self._metrics.items():
                metric.reset_states()

            epoch_t2 = time.time()
            self.print("Epoch {}. Total Time: {:.4f}s".format(epoch, epoch_t2 - epoch_t1))
        end_time = time.time()
        self.print("Training cost total:{:.2f}s".format(end_time - begin_time))

    def test(self, test_dataset, ckpt_path):
        """TODO: Docstring for test.

        Args:
            test_dataset: The dataset to test.

        Returns: TODO

        """
        fmt = "Test step {}, cost {:.4f}s. metrics: {}"
        self.print("Begin Test for {}".format(ckpt_path))
        self.print("load weight from : {}".format(ckpt_path))

        for (inp, tar) in test_dataset.take(1):
            self.test_step(inp, tar)
            self.ckpt.restore(ckpt_path)

        [metric.reset_states() for k, metric in self._metrics.items()]
        epoch = os.path.basename(ckpt_path)[8:11]
        for callback in self.callbacks:
            callback.epoch = epoch

        for step, (inp, tar) in enumerate(test_dataset):
            t1 = time.time()
            result = self.test_step(inp, tar)
            logs = self.logs.copy()
            logs["result"] = result
            [callback.on_test_batch_end(step, logs) for callback in self.callbacks]
            t2 = time.time()
            self.print(fmt.format(step, t2 - t1, self.print_metrics(self._val_metrics)))
        self.print("Test Done.")
