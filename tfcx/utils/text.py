# -*- coding: utf-8 -*-
#================================================================
#   God Bless You.
#
#   author: klaus
#   email: chengfeng2333@gmail.com
#   created date: 2019/12/08
#   description:
#
#================================================================
import os

CURRENT_FILE_DIRECTORY = os.path.abspath(os.path.dirname(__file__))

chars1 = "Υǒ◆⒐§◎≈"
chars2 = "Yo·9$〇="

REPLACE_DICT = {}
UNKNOWN_CHAR = "卍"


def _load_replace_dict():
    replace_dict_file = os.path.join(CURRENT_FILE_DIRECTORY, "./char_replace_dict.txt")
    replace_dict = {}
    for line in open(replace_dict_file, "r"):
        line = line.replace("\n", "")
        k, v = line.split(" ", 1)
        replace_dict[k] = v

    # load chars1 -> chars2
    for i, k in enumerate(chars1):
        replace_dict[k] = chars2[i]

    return replace_dict


VOCAB_DICT = set()


def replace_unknow_chars(text, remove_space=True, vocab_file=None, vocab=None):
    """replace chars in text which is not in vocab_file with 卍.
    The texts are first replaced by the `replaceFullToHalf`
    Args:
        text: str. The text to replace.
        remove_space: Boolean. Whether to replace the space in the text.
        vocab_file: str. The file path of the vocab_file
        vocab: None or set. The vocabulary.

    Returns: str. replaced text.

    """
    global VOCAB_DICT

    if vocab is None and vocab_file is None:
        raise ValueError("at least vocab or vocab_file is not None")
    if len(VOCAB_DICT) == 0:
        if vocab is not None:
            VOCAB_DICT = set(vocab)
        else:
            VOCAB_DICT = set(open(vocab_file, "r").read().splitlines())
    if remove_space:
        text = text.replace(" ", "")
        text = text.replace("　", "")
    text = replace_chars(text)
    new_text = ""
    for char in text:
        if char not in VOCAB_DICT:
            new_text += "卍"
        else:
            new_text += char
    return new_text


def replace_chars(text):
    """replace chars in text.

    Args:
        text: str.

    Returns: str. Replaced text.

    """
    global REPLACE_DICT
    if len(REPLACE_DICT) == 0:
        REPLACE_DICT = _load_replace_dict()
    new_str = ""
    for char in text:
        if char in REPLACE_DICT:
            new_str += REPLACE_DICT[char]
        else:
            new_str += char
    return new_str
