import tensorflow as tf
from .residual_block import build_res_block_1


class ResNet34(tf.keras.Model):

    def __init__(self,
                 include_head=True,
                 strides=[2, 2, 2, 2, 2],
                 include_pooling=True,
                 num_classes=1000):
        super(ResNet34, self).__init__()

        self.include_head = include_head
        self.include_pooling = include_pooling

        self.pre1 = tf.keras.layers.Conv2D(filters=64,
                                           kernel_size=(7, 7),
                                           strides=strides[0],
                                           padding='same')
        self.pre2 = tf.keras.layers.BatchNormalization()
        self.pre3 = tf.keras.layers.Activation(tf.keras.activations.relu)
        if include_pooling:
            self.pre4 = tf.keras.layers.MaxPool2D(pool_size=(3, 3), strides=strides[1])

        self.layer1 = build_res_block_1(filter_num=64, blocks=3)
        self.layer2 = build_res_block_1(filter_num=128, blocks=4, stride=strides[2])
        self.layer3 = build_res_block_1(filter_num=256, blocks=6, stride=strides[3])
        self.layer4 = build_res_block_1(filter_num=512, blocks=3, stride=strides[4])

        if include_head:
            self.avgpool = tf.keras.layers.GlobalAveragePooling2D()
            self.fc = tf.keras.layers.Dense(units=num_classes,
                                            activation=tf.keras.activations.softmax)

    def call(self, inputs, training=None, mask=None):
        x = self.pre1(inputs)
        x = self.pre2(x, training=training)
        x = self.pre3(x)
        if self.include_pooling:
            x = self.pre4(x)
        l1 = self.layer1(x, training=training)
        l2 = self.layer2(l1, training=training)
        l3 = self.layer3(l2, training=training)
        l4 = self.layer4(l3, training=training)
        if self.include_head:
            avgpool = self.avgpool(l4)
            out = self.fc(avgpool)
        else:
            out = l4
        return out
