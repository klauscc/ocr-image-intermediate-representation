# -*- coding: utf-8 -*-
#================================================================
#   God Bless You.
#
#   author: klaus
#   email: chengfeng2333@gmail.com
#   created date: 2019/12/08
#   description:
#
#================================================================

import tensorflow as tf


def resize_single_image(image,
                        size,
                        min_img_side=10,
                        max_img_side=-1,
                        method=tf.image.ResizeMethod.BILINEAR,
                        resize_with_pad=False,
                        antialias=False):
    """resize one image with size support None. None means calculated by keep 
    aspect ratio. 

    For example, given the original image of size [300, 400], 
    if size is [150,None], the image will be resized to [150, 200].
    if size is [None, 200], the image will be resized to [150, 200].
    if size is [a, b] and a/b is not None, the image will be resized to [a, b]. 


    Args:
        image: 3-D Tensor of shape [height, width, channels] or 4-D Tensor of
            shape [batch, height, width, channels].
            size: List of two element: `new_height`, `new_width`. One of which should
                be `None`.

    Kwargs:
        min_img_side: Int. Limit the minimum side of image to the value. Default is 10.
        max_img_side: Int. Limit the max image side to the value. Default None.
        antialias: Boolean. Whether to use an anti-aliasing filter when downsampling 
            an image.

    Returns: Resized image.

    """
    image = tf.cast(image, tf.float32)
    init_hw = tf.shape(image)
    init_h, init_w = init_hw[0], init_hw[1]

    if init_h < min_img_side or init_w < min_img_side:
        if init_h < min_img_side:
            init_h = min_img_side
        else:
            init_w = min_img_side
        protection_size = (init_h, init_w)
        image = tf.image.resize(image, protection_size)

    init_h = tf.cast(init_h, tf.float32)
    init_w = tf.cast(init_w, tf.float32)
    new_h, new_w = size[0], size[1]
    if new_h is None and new_w is None:
        raise ValueError("size should not be [None, None]")

    if new_h is not None and new_w is not None:
        pass
    elif new_h is not None:
        new_w = tf.cast(init_w * new_h / init_h, tf.int32)
        # if max_img_side > 0 and new_w > max_img_side:
        # new_w = max_img_side
        size = (new_h, new_w)
    else:
        new_h = tf.cast(init_h * new_w / init_w, tf.int32)
        # if max_img_side > 0 and new_h > max_img_side:
        # new_h = max_img_side
        size = (new_h, new_w)

    if resize_with_pad:
        return tf.image.resize_with_pad(image, new_h, new_w, method, antialias)
    else:
        return tf.image.resize(image, size, method, antialias=antialias)
