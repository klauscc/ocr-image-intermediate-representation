#================================================================
#   God Bless You. 
#   
#   author: klaus
#   email: chengfeng2333@gmail.com
#   created date: 2019/12/10
#   description: 
#
#================================================================

name=`basename "$0"`

workspace=$HOME/stor6/workspace/003-790-text_reconstruct/$name
python bin/train.py --workspace $workspace \
    --params params/pix2pix-mask_loss.yml \
    --resize_with_pad True 
