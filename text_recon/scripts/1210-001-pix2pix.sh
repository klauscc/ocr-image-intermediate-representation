#================================================================
#   God Bless You. 
#   
#   author: klaus
#   email: chengfeng2333@gmail.com
#   created date: 2019/12/10
#   description: 
#
#================================================================

name=`basename "$0"`

python bin/train.py --workspace ~/stor6/workspace/003-790-text_reconstruct/$name \
    --params params/pix2pix.yml
