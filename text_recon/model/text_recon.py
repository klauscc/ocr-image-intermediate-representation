# -*- coding: utf-8 -*-
#================================================================
#   God Bless You.
#
#   author: klaus
#   email: chengfeng2333@gmail.com
#   created date: 2019/12/10
#   description:
#
#================================================================

import tensorflow as tf
keras = tf.keras

from tfcx.network import unet
from tfcx.sequence import seq_text

from .transformer.model import Transformer


class TextReconModel(object):
    """text reconstruction model"""

    def __init__(self, params):
        super(TextReconModel, self).__init__()
        self.params = params

    def create_generator(self):
        """create the image generator
        Returns: TODO

        """
        params = self.params
        generator = unet.Pix2PixUnet(params.filters,
                                     output_channels=3,
                                     kernel_size=params.kernel_size,
                                     num_downsample=params.num_downsample)
        return generator

    def create_discriminator(self):
        """discriminate the generated image is True or False.
        Returns: TODO

        """
        initializer = tf.random_normal_initializer(0., 0.02)

        inp = keras.layers.Input(shape=[None, None, 3], name="input_image")
        tar = keras.layers.Input(shape=[None, None, 3], name="target_image")
        x = keras.layers.concatenate([inp, tar])

        params = self.params
        filters = params.filters
        kernel_size = params.kernel_size

        down1 = unet.downsample(filters, kernel_size, apply_norm=False)(x)
        down2 = unet.downsample(filters * 2, kernel_size)(down1)
        down3 = unet.downsample(filters * 4, kernel_size)(down2)

        zero_pad1 = keras.layers.ZeroPadding2D()(down3)
        conv = keras.layers.Conv2D(filters * 8,
                                   kernel_size,
                                   strides=1,
                                   kernel_initializer=initializer,
                                   use_bias=False)(zero_pad1)

        batchnorm1 = keras.layers.BatchNormalization()(conv)

        leaky_relu = keras.layers.LeakyReLU()(batchnorm1)

        # zero_pad2 = keras.layers.ZeroPadding2D()(leaky_relu)
        # last = keras.layers.Conv2D(1, kernel_size, strides=1,
        # kernel_initializer=initializer)(zero_pad2)

        fc = keras.layers.GlobalMaxPooling2D()(leaky_relu)
        last = keras.layers.Dense(1)(fc)

        return keras.Model(inputs=[inp, tar], outputs=last)

    def create_transformer(self):
        """transformer is used to ensure the text in generated image not changed.
        Returns: Transformer model

        """
        params = self.params
        tparams = params.transformer
        vocab_size = len(seq_text.read_vocab_file(params.vocab_file))
        tparams.vocab_size = vocab_size
        transformer = Transformer(tparams.num_layers,
                                  tparams.d_model,
                                  tparams.num_heads,
                                  tparams.dff,
                                  input_vocab_size=tparams.vocab_size,
                                  target_vocab_size=tparams.vocab_size,
                                  pe_input=tparams.vocab_size,
                                  pe_target=tparams.vocab_size,
                                  rate=tparams.dropout_rate)
        return transformer
