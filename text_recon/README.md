text representation.

## 1. Dependencies

```
tensorflow-gpu==2.0.0
```

## 2. Usage

### 2.1 add modules to PYHTONPATH

```
source prepare_env.sh
```

### 2.2 train using scripts

```
bash ./scripts/1211-003-pix2pix-gen_mask.sh

# <workspace> is the location where log file and checkpoint will be saved to and is specified in the previous file.
tensorboard --logdir <workspace>/tensorboard --ip 0.0.0.0 --port 14000
```
