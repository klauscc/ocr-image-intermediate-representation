# -*- coding: utf-8 -*-
#================================================================
#   God Bless You.
#
#   author: klaus
#   email: chengfeng2333@gmail.com
#   created date: 2019/12/10
#   description:
#
#================================================================

import tensorflow as tf
keras = tf.keras
from tfcx.trainer import BasicTrainer
from tfcx.sequence.seq_text import Tokenizer

from .model.transformer.model import create_masks


class TextReconTrainer(BasicTrainer):
    """Trainer for text recongnition"""

    def __init__(self, params, text_recon_model):
        self.params = params

        self.generator = text_recon_model.create_generator()
        self.discriminator = text_recon_model.create_discriminator()
        self.text_recognizer = text_recon_model.create_transformer()

        self.tokenizer = Tokenizer(params.vocab_file)

        self.gen_disc_loss_object = keras.losses.BinaryCrossentropy(from_logits=True,
                                                                    reduction="none")

        self.text_recognizer_loss_object = keras.losses.SparseCategoricalCrossentropy(
            from_logits=True, reduction="none")
        self.define_optimizers()

        self.tokenizer.initialize()

        x_shape = [None] + params.img_size + [3]
        self.input_signature = [{
            "x": tf.TensorSpec(shape=x_shape, dtype=tf.float32),
            "img_shape": tf.TensorSpec([None, 2], dtype=tf.int32),
            "x_mask": tf.TensorSpec(shape=x_shape, dtype=tf.float32),
            "file_paths": tf.TensorSpec([None, 4], dtype=tf.string),
        }, {
            "gt_x": tf.TensorSpec(shape=x_shape, dtype=tf.float32),
            "gt_x_mask": tf.TensorSpec(shape=x_shape, dtype=tf.float32),
            "label": tf.TensorSpec([None, None], dtype=tf.string),
            "tokenized_label_pad_eos": tf.TensorSpec([None, None], dtype=tf.int64),
        },
                                tf.TensorSpec(shape=[], dtype=tf.int64)]

        self.train_step = tf.function(self.train_step, input_signature=self.input_signature)

        super(TextReconTrainer, self).__init__(params)

    def define_optimizers(self):
        params = self.params
        self.generator_optimizer = keras.optimizers.Adam(params.generator.optim.lr,
                                                         params.generator.optim.beta_1,
                                                         params.generator.optim.beta_2)

        self.discriminator_optimizer = keras.optimizers.Adam(params.discriminator.optim.lr,
                                                             params.discriminator.optim.beta_1,
                                                             params.discriminator.optim.beta_2)
        text_recognizer_lr = self.text_recognizer.lr_scheduler(
            warmup_steps=params.transformer.optim.warmup_steps)
        self.text_recognizer_optimizer = keras.optimizers.Adam(text_recognizer_lr,
                                                               params.transformer.optim.beta_1,
                                                               params.transformer.optim.beta_2)

    def define_ckpt(self):
        self.ckpt = tf.train.Checkpoint(
            generator=self.generator,
            discriminator=self.discriminator,
            text_recognizer=self.text_recognizer,
            generator_optimizer=self.generator_optimizer,
            discriminator_optimizer=self.discriminator_optimizer,
            text_recognizer_optimizer=self.text_recognizer_optimizer,
        )

    def generator_loss(self, disc_generated_output, gen_output, target, target_mask, text_loss):
        gan_loss = self.gen_disc_loss_object(tf.ones_like(disc_generated_output),
                                             disc_generated_output)    # [bs, dh, dw]
        gan_loss = tf.reduce_mean(gan_loss)

        # mean absolute error
        l1_loss = tf.abs(target - gen_output)

        if self.params.loss.apply_mask:
            l1_text_loss = l1_loss * target_mask
            l1_text_loss = tf.reduce_sum(l1_text_loss) / tf.reduce_sum(target_mask)
            bg_mask = 1 - target_mask
            l1_bg_loss = l1_loss * bg_mask
            l1_bg_loss = tf.reduce_sum(l1_bg_loss) / tf.reduce_sum(bg_mask)
            l1_loss = l1_text_loss * self.params.loss.text_weight + l1_bg_loss
        else:
            l1_loss = tf.reduce_mean(l1_loss)

        total_gen_loss = gan_loss + (self.params.loss.LAMBDA *
                                     l1_loss) + (self.params.loss.transformer_beta * text_loss)

        return total_gen_loss, gan_loss, l1_loss

    def discriminator_loss(self, disc_real_output, disc_generated_output):
        loss_object = self.gen_disc_loss_object

        real_loss = loss_object(tf.ones_like(disc_real_output), disc_real_output)
        real_loss = tf.reduce_mean(real_loss)

        generated_loss = loss_object(tf.zeros_like(disc_generated_output), disc_generated_output)
        generated_loss = tf.reduce_mean(generated_loss)

        total_disc_loss = real_loss + generated_loss

        return total_disc_loss

    def create_gen_disc_mask(self, x_img_shape, input_img, disc_output):
        """create mask for loss calculation of generator and discriminator.

        Args:
            x_img_shape: 2-D Tensor of shape [bs, 2]. The last dimention is [oh, ow] of the original image. 
            input_img: 3-D Tensor of shape [bs, h, w, c].
            disc_output: 4-D Tensor of shape [bs, dh, dw, dc].

        Returns: TODO

        """
        gen_masks = []
        disc_masks = []
        bs = tf.shape(x_img_shape)[0]
        iw = tf.shape(input_img)[2]
        dh = tf.shape(disc_output)[1]
        dw = tf.shape(disc_output)[2]
        for i in range(bs):
            oh = x_img_shape[i][0]
            ow = x_img_shape[i][1]
            gen_mask = tf.concat([tf.ones([oh, ow]), tf.zeros([oh, iw - ow])], axis=1)
            gen_masks.append(gen_mask)
            dow = ow * dh / oh
            disc_mask = tf.concat([tf.ones([dh, dow]), tf.zeros([dh, dw - dow])], axis=1)
            disc_masks.append(disc_masks)
        return tf.stack(gen_masks), tf.stack(disc_masks)

    def text_recognizer_loss(self, grad_tape, inp, tar):
        tar_inp = tar[:, :-1]
        tar_real = tar[:, 1:]
        bs = tf.shape(tar)[0]
        inp_len = self.params.img_size[1] // 8
        inp_mask = tf.ones([bs, inp_len])
        enc_padding_mask, combined_mask, dec_padding_mask = create_masks(inp_mask, tar_inp)

        predictions, _ = self.text_recognizer(inp, tar_inp, True, enc_padding_mask, combined_mask,
                                              dec_padding_mask)

        loss_mask = tf.math.logical_not(tf.math.equal(tar_real, 0))
        loss = self.text_recognizer_loss_object(tar_real, predictions)
        loss_mask = tf.cast(loss_mask, loss.dtype)
        loss *= loss_mask
        return tf.reduce_mean(loss), predictions

    def train_step(self, inp, tar, step):
        input_img = inp["x"]
        target_img = inp["x_mask"]
        target_mask = inp["x_mask"]
        with tf.GradientTape() as gen_tape, tf.GradientTape() as disc_tape, tf.GradientTape(
        ) as text_tape:
            gen_output = self.generator(input_img, training=True)

            # threshold
            threshed_gen_output = tf.where(gen_output < 0.5, tf.zeros_like(gen_output),
                                           tf.ones_like(gen_output))

            # text_recognition

            disc_real_output = self.discriminator([input_img, target_img], training=True)
            disc_generated_output = self.discriminator([input_img, threshed_gen_output],
                                                       training=True)

            # log generator generated_output

            # text recognizer
            gen_output.set_shape(input_img.shape)
            text_loss, text_predictions = self.text_recognizer_loss(text_tape, gen_output,
                                                                    tar["tokenized_label_pad_eos"])
            # text_real = self.tokenizer.detokenize(tf.cast(tar["tokenized_label_pad_eos"], tf.int64))
            # text_predictions_ids = tf.argmax(text_predictions, axis=-1)
            # text_pre = self.tokenizer.detokenize(text_predictions_ids)

            if step % 100 == 0:
                pass
                # tf.summary.image("input", input_img, max_outputs=4, step=step)
                # tf.summary.image("generated", gen_output, max_outputs=4, step=step)
                # tf.summary.image("generated_thresh", threshed_gen_output, max_outputs=4, step=step)
                # tf.summary.image("gt", target_img, max_outputs=4, step=step)
                # tf.summary.text("text_real", text_real[:4], step=step)
                # tf.summary.text("text_pre", text_pre[:4], step=step)
                # tf.summary.image("gt_mask", tar["gt_x_mask"], max_outputs=4, step=step)

            gen_total_loss, gen_gan_loss, gen_l1_loss = self.generator_loss(
                disc_generated_output, gen_output, target_img, target_mask, text_loss)
            disc_loss = self.discriminator_loss(disc_real_output, disc_generated_output)

            generator_gradient = gen_tape.gradient(gen_total_loss,
                                                   self.generator.trainable_variables)
            discriminator_gradient = disc_tape.gradient(disc_loss,
                                                        self.discriminator.trainable_variables)

            self.generator_optimizer.apply_gradients(
                zip(generator_gradient, self.generator.trainable_variables))
            self.discriminator_optimizer.apply_gradients(
                zip(discriminator_gradient, self.discriminator.trainable_variables))

            text_recognizer_gradient = text_tape.gradient(text_loss,
                                                          self.text_recognizer.trainable_variables)
            self.text_recognizer_optimizer.apply_gradients(
                zip(text_recognizer_gradient, self.text_recognizer.trainable_variables))

            self.train_metric("loss")(gen_total_loss)
            tf.summary.scalar('text_loss', text_loss, step=step)
            tf.summary.scalar('gen_total_loss', gen_total_loss, step=step)
            tf.summary.scalar('gen_gan_loss', gen_gan_loss, step=step)
            tf.summary.scalar('gen_l1_loss', gen_l1_loss, step=step)
            tf.summary.scalar('disc_loss', disc_loss, step=step)

    def test_step(self, inp, tar):
        pass
