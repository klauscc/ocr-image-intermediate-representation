# -*- coding: utf-8 -*-
#================================================================
#   God Bless You.
#
#   author: klaus
#   email: chengfeng2333@gmail.com
#   created date: 2019/12/08
#   description:
#
#================================================================

import os
import glob
import numpy as np
import tensorflow as tf
from functools import partial

import tfcx.nn as cxnn
import tfcx.utils.text as cxtext
import tfcx.sequence.seq_text as cxseq


class BaseDataset(object):
    """the base dataset

    Args:
        params: EasyDict. The parameters.

    """

    def __init__(self, params):
        super(BaseDataset, self).__init__()
        self.params = params

        self.tokenizer = cxseq.Tokenizer(params.vocab_file)

    def get_images_path(self, data_dir, filename):
        """return the images given filename.

        Args:
            data_dir: String. The directory of the data.
            filename: String. The filename.

        Returns: List of String. Path of 4 images: 
                (augmented_image, gt_image, augmented_mask, gt_mask)

        """
        subdirs = ["augmented", "gt", "augmented_mask", "gt_mask"]
        images_path = []
        for subdir in subdirs:
            images_path.append(os.path.join(data_dir, subdir, filename))
        return images_path


class LabelFileDataset(BaseDataset):
    """Read examples from directory"""

    def __init__(self, params):
        super(LabelFileDataset, self).__init__(params)
        self.data_dir = params.data_dir
        self.label_file = params.label_file

    def input_fn(self, mode="TRAIN", batch_size=32, **kwargs):
        """TODO: Docstring for input_fn.

        Args:
            **kwargs (TODO): TODO

        Kwargs:
            mode: String. Either "TRAIN" or "EVAL".
            batch_size: Int. Batch size. 

        Returns: tf.data.Dataset.
            The dataset iterator is of the following output:
                        ({
                            "x": batched_img_shape,
                            "img_shape": [2],
                            "gt_x": batched_img_shape,
                            "x_mask": batched_img_shape,
                            "gt_x_mask": batched_img_shape,
                            "file_paths": [4],
                        }, {
                            "label": [None],
                            "tokenized_label_pad_eos": [None],
                        }))

        """
        file_paths, labels = self.get_image_pairs(self.data_dir, self.label_file)
        n_samples = len(labels)
        self.tokenizer.initialize()

        file_paths = tf.constant(file_paths, dtype=tf.string)
        labels = tf.ragged.constant(labels, dtype=tf.string)    # ragged tensor
        dataset = tf.data.Dataset.range(n_samples)
        if mode.upper() == "TRAIN":
            dataset = dataset.shuffle(buffer_size=n_samples)

        parse_func = partial(self.parse_example, file_paths=file_paths, labels=labels)
        dataset = dataset.map(parse_func, num_parallel_calls=8)
        if batch_size > 0:
            dataset = self.batch(dataset, batch_size, self.params.img_size, 3)
        return dataset

    def parse_example(self, id, file_paths, labels):
        """parse example from given example id.

        Args:
            id: The example id.
            file_paths: String Tensor of shape [n_examples, n_images]. All the file_paths of the dataset.
                Each example contains `n_images`: augmented, gt, augmented_mask, gt_mask. 
            labels: String Ragged Tensor. Each row is a label.

        Returns: TODO

        """
        img_paths = file_paths[id]
        label = labels[id]

        # imgs
        input_img = self.process_img(img_paths[0])
        img_shape = tf.shape(input_img)[:2]
        gt_img = self.process_img(img_paths[1], resized_shape=img_shape)
        input_img_mask = self.process_img(img_paths[2], resized_shape=img_shape)
        gt_img_mask = self.process_img(img_paths[3], resized_shape=img_shape)
        # label
        tokenized_label_pad_eos = self.process_label(label)

        inputs = {
            "x": input_img,
            "img_shape": img_shape,
            "x_mask": input_img_mask,
            "file_paths": img_paths,
        }
        targets = {
            "gt_x": gt_img,
            "gt_x_mask": gt_img_mask,
            "label": label,
            "tokenized_label_pad_eos": tokenized_label_pad_eos,
        }
        return (inputs, targets)

    def batch(self, dataset, batch_size, batched_img_size, channel):
        """batch examples.

        Args:
            dataset: tf.data.Dataset. The dataset to be batched.
            batch_size: int. batch size.
            batched_img_size: List. The img size to be batched. e.g. [32, None]. 
            channel: Int. 3 for RGB and 1 for GRAY.

        Returns: batched dataset.

        """
        batched_img_shape = list(batched_img_size) + [channel]
        dset = dataset.padded_batch(batch_size,
                                    padded_shapes=({
                                        "x": batched_img_shape,
                                        "img_shape": [2],
                                        "x_mask": batched_img_shape,
                                        "file_paths": [4],
                                    }, {
                                        "gt_x": batched_img_shape,
                                        "gt_x_mask": batched_img_shape,
                                        "label": [None],
                                        "tokenized_label_pad_eos": [None],
                                    }))
        return dset

    def process_img(self, img_path, resized_shape=None):
        """read and process img_img(self, img_path,_img(self, img_path, resiz

        Args:
            img_path: String Scalar Tensor. The image path of the file.

        Returns: The processed img.

        """
        img = tf.io.read_file(img_path)
        img = tf.image.decode_jpeg(img, channels=3)
        if resized_shape is None:
            resized_shape = self.params.img_size
        img = cxnn.image.resize_single_image(img,
                                             size=resized_shape,
                                             max_img_side=self.params.max_img_side,
                                             resize_with_pad=self.params.resize_with_pad)
        img = img / 255.
        return img

    def process_label(self, label):
        """process label

        Args:
            label: 1-D Tensor String.

        Returns: 1-D Tensor int64.
            Tokenized label. The PAD_ID and EOS_ID is padded at the begining and the end of the label.

        """
        tokenized_label = self.tokenizer.tokenize(label)
        tokenized_label_pad_eos = tf.concat([[cxseq.PAD_ID], tokenized_label, [cxseq.EOS_ID]],
                                            axis=0)
        return tokenized_label_pad_eos

    def get_image_pairs(self, data_dir, label_file, delimiter=','):
        """TODO: Docstring for get_image_pairs.

        Args:
            data_dir: String. The path to save the data.
            label_file: String. The file to save the label.

        Returns: Tuple. (file_paths, labels). `file_paths` are the image paths
            and `labels` are the corresponding labels. The two list are of the 
            same length. 
                The label are splited to list. For example, label "hello" are
            splited to ["h", "e", "l", "l", "o"]. `labels` are list of labels

        """
        with open(label_file, "r") as f:
            lines = f.read().splitlines()
        file_paths, labels = [], []
        for line in lines:
            filename, label = line.split(delimiter, maxsplit=1)
            label = cxtext.replace_unknow_chars(label, vocab_file=self.params.vocab_file)
            file_paths.append(self.get_images_path(data_dir, filename))
            labels.append(list(label))
        return file_paths, labels
