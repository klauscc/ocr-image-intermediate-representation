# -*- coding: utf-8 -*-
#================================================================
#   God Bless You.
#
#   author: klaus
#   email: chengfeng2333@gmail.com
#   created date: 2019/12/09
#   description:
#
#================================================================

import os

CURRENT_FILE_DIRECTORY = os.path.abspath(os.path.dirname(__file__))

data_dir = "/tmp/fengcheng/dataset/ocr/english_words_50w"
label_files = ["english_words_50w_label_val.txt", "english_words_50w_label_train.txt"]

vocab = set()
for label_f in label_files:
    f_path = os.path.join(data_dir, label_f)
    lines = open(f_path, "r").read().splitlines()
    for line in lines:
        filename, label = line.split(",", 1)
        for c in label:
            vocab.add(c)

vocab = sorted(list(vocab))

save_path = os.path.join(CURRENT_FILE_DIRECTORY, "../data/vocab.txt")
with open(save_path, "w") as f:
    for c in vocab:
        f.write(c + "\n")
