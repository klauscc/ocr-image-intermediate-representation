# -*- coding: utf-8 -*-
#================================================================
#   God Bless You.
#
#   author: klaus
#   email: chengfeng2333@gmail.com
#   created date: 2019/12/10
#   description:
#
#================================================================

import tensorflow as tf

from tfcx.param_helper import get_params
from text_recon.dataset import ocr_dataset
from text_recon.text_trainer import TextReconTrainer

# params
epochs = 200
params = get_params()
params.print_fn(params)

# dataset
train_dataset = ocr_dataset.LabelFileDataset(params)
train_db = train_dataset.input_fn(mode="TRAIN", batch_size=params.batch_size)

# model
from text_recon.model.text_recon import TextReconModel
text_recon_model = TextReconModel(params)

# trainer
trainer = TextReconTrainer(params, text_recon_model)

trainer.train(train_db, epochs=epochs)
