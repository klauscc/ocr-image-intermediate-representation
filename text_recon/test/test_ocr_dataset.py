# -*- coding: utf-8 -*-
#================================================================
#   God Bless You.
#
#   author: klaus
#   email: chengfeng2333@gmail.com
#   created date: 2019/12/09
#   description:
#
#================================================================
import os
import time

CURRENT_FILE_DIRECTORY = os.path.abspath(os.path.dirname(__file__))

from text_recon.dataset import ocr_dataset
from tfcx.param_helper import get_params

default_params = {
    "vocab_file": os.path.join(CURRENT_FILE_DIRECTORY, "../data/vocab.txt"),
    "img_size": [32, None],
    "data_dir": "/tmp/fengcheng/dataset/ocr/english_words_50w",
    "label_file": "/tmp/fengcheng/dataset/ocr/english_words_50w/english_words_50w_label_train.txt",
    "batch_size": 64,
}

params = get_params(default_params)

dataset = ocr_dataset.LabelFileDataset(params)

dset = dataset.input_fn(mode="TRAIN", num_epoch=1, batch_size=64)
t = time.time()
for i, (inputs, outputs) in enumerate(dset):
    print(inputs["x"].shape)
    print(outputs["tokenized_label_pad_eos"].shape, outputs["tokenized_label_pad_eos"][0])
    step_t = time.time() - t
    t = time.time()
    print("step {}. step_t:{:.5f}s.".format(i, step_t))
