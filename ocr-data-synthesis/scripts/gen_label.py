# -*- coding: utf-8 -*-
#================================================================
#   God Bless You.
#
#   file name: bin_generate_label.py
#   author: klaus
#   email: klaus.cheng@qq.com
#   created date: 2019/05/09
#   description:
#
#================================================================

import os
import sys
import glob
from functools import partial
from multiprocessing import Pool

from utils.label_replace import replaceFullToHalf

CURRENT_FILE_DIRECTORY = os.path.abspath(os.path.dirname(__file__))

DICT = open(os.path.join(CURRENT_FILE_DIRECTORY, "../charset/clean_charset_4pd.txt"),
            "r").read().splitlines()[0]

UNKNOW_CHARS = set()


def parse_text_files(text_file, rm_space=True, delimiter=" "):
    filename_label = {}
    for line in open(text_file, "r"):
        line = line.replace("\n", "")
        file_name, label = line.split(delimiter, 1)

        label = label.replace("　", " ").strip()

        if rm_space:
            label = label.replace(" ", "")
        else:
            label = " ".join(label.split())

        label = replaceFullToHalf(label)

        new_label = ""
        for char in label:
            if char not in DICT:
                UNKNOW_CHARS.add(char)
                new_label += "卍"
            else:
                new_label += char
        if new_label != "":
            filename_label[file_name] = new_label
    return filename_label


def generate_label(text_file,
                   img_dir,
                   save_path,
                   filename_label=None,
                   write_mode="w",
                   rm_space=True,
                   delimiter=" ",
                   split=True):
    if filename_label is None:
        filename_label = parse_text_files(text_file, rm_space=rm_space)

    def get_files_in_dir(dire):
        img_paths = glob.glob(os.path.join(dire, "*.jpg"))
        filenames = []
        for p in img_paths:
            head, f = os.path.split(p)
            filenames.append(f)
        return set(filenames)

    subdirs = ["augmented", "gt", "augmented_mask", "gt_mask"]
    imgs = get_files_in_dir(os.path.join(img_dir, subdirs[0]))
    for i in range(1, len(subdirs)):
        imgs = imgs.intersection(get_files_in_dir(os.path.join(img_dir, subdirs[i])))
    print("{} images found in {}".format(len(imgs), img_dir))

    train_file_path = save_path.replace(".txt", "_train.txt")
    val_file_path = save_path.replace(".txt", "_val.txt")

    with open(train_file_path, write_mode) as tf, open(val_file_path, write_mode) as vf:
        for i, img_path in enumerate(imgs):
            head, filename = os.path.split(img_path)
            head, subdir = os.path.split(head)
            relative_path = os.path.join(subdir, filename)
            if filename in filename_label:
                label = filename_label[filename]
                if i % 10 == 0:
                    vf.write("{},{}\n".format(relative_path, label))
                else:
                    tf.write("{},{}\n".format(relative_path, label))


def icdar():
    text_file = os.path.join(CURRENT_FILE_DIRECTORY, "../data/texts/icdar20W.txt")
    img_dir = "/home/chengfeng/datasets/cf_generated/icdar_like"
    save_path = "/home/chengfeng/datasets/cf_generated/icdar_like_label.txt"
    generate_label(text_file, img_dir, save_path)


def qingjie80W():
    text_file = os.path.join(CURRENT_FILE_DIRECTORY, "../data/texts/qingjie80W.txt")
    img_dir = "/home/chengfeng/datasets/cf_generated/qingjie_80W"
    save_path = "/home/chengfeng/datasets/cf_generated/qingjie_80W_label.txt"
    generate_label(text_file, img_dir, save_path)


def qingjie80W_1():
    text_file = os.path.join(CURRENT_FILE_DIRECTORY, "../data/texts/qingjie80W.txt")
    img_dir = "/home/chengfeng/datasets/cf_generated/qingjie_80W_1"
    save_path = "/home/chengfeng/datasets/cf_generated/qingjie_80W_1_label.txt"
    generate_label(text_file, img_dir, save_path)


def long_text():
    text_file = os.path.join(CURRENT_FILE_DIRECTORY, "../data/texts/longtext20W.txt")
    img_dir = "/home/chengfeng/datasets/cf_generated/long_text"
    save_path = "/home/chengfeng/datasets/cf_generated/long_text_label.txt"
    generate_label(text_file, img_dir, save_path)


def normal():
    img_dir = "/home/chengfeng/datasets/cf_generated/normal"
    img_dirs = glob.glob(img_dir + "/*")
    print(img_dirs)

    text_file = os.path.join(CURRENT_FILE_DIRECTORY, "../data/texts/normal600W.txt")
    filename_label = parse_text_files(text_file)

    for dir in img_dirs:
        if os.path.isdir(dir):
            head, dir_name = os.path.split(dir)
            save_path = "/home/chengfeng/datasets/cf_generated/normal/normal_label.txt"
            generate_label(text_file, dir, save_path, filename_label, write_mode="a")


def tight_bound():
    text_file = os.path.join(CURRENT_FILE_DIRECTORY, "../data/texts/normal600W/normal600W_14.txt")
    img_dir = "/home/chengfeng/datasets/cf_generated/tight_bound"
    save_path = "/home/chengfeng/datasets/cf_generated/tight_bound_label.txt"
    generate_label(text_file, img_dir, save_path)


def tight_numberletters():
    text_file = os.path.join(CURRENT_FILE_DIRECTORY, "../data/texts/numbers30w.txt")
    img_dir = "/home/chengfeng/datasets/cf_generated/tight_numberletters"
    save_path = "/home/chengfeng/datasets/cf_generated/tight_numberletters_label.txt"
    generate_label(text_file, img_dir, save_path)


def tight_english():
    text_file = os.path.join(CURRENT_FILE_DIRECTORY, "../data/texts/english_words_10w.txt")
    img_dir = "/home/chengfeng/datasets/cf_generated/tight_english"
    save_path = "/home/chengfeng/datasets/cf_generated/tight_english_label.txt"
    generate_label(text_file, img_dir, save_path)


def tight_english_50w():
    text_file = os.path.join(CURRENT_FILE_DIRECTORY, "../data/texts/english_words_50w.txt")
    img_dir = "/home/chengfeng/datasets/cf_generated/tight_english50w"
    save_path = "/home/chengfeng/datasets/cf_generated/tight_english_50w_label.txt"
    generate_label(text_file, img_dir, save_path, rm_space=True)


def receipt_text():
    text_file = os.path.join(CURRENT_FILE_DIRECTORY, "../data/texts/receipt50W.txt")
    img_dir = "/home/chengfeng/datasets/cf_generated/receipt_text"
    save_path = "/home/chengfeng/datasets/cf_generated/receipt_text_label.txt"
    generate_label(text_file, img_dir, save_path, rm_space=True)


def receipt_number():
    text_file = os.path.join(CURRENT_FILE_DIRECTORY, "../data/texts/receipt_number30w.txt")
    img_dir = "/home/chengfeng/datasets/cf_generated/receipt_number"
    save_path = "/home/chengfeng/datasets/cf_generated/receipt_number_label.txt"
    generate_label(text_file, img_dir, save_path, rm_space=True)


def narrow_english_text150w():
    text_file = os.path.join(CURRENT_FILE_DIRECTORY, "../data/texts/english_text_150w_1.txt")
    img_dir = "/home/chengfeng/datasets/cf_generated/english150w_with_space"
    save_path = "/home/chengfeng/datasets/cf_generated/english150w_with_space_label.txt"
    generate_label(text_file, img_dir, save_path, rm_space=False)


def english_50w():
    text_file = os.path.join(CURRENT_FILE_DIRECTORY, "../data/texts/english_words_50w.txt")
    # img_dir = "/home/fengchan/stor6/dataset/ocr/english_words_50w"
    # save_path = "/home/fengchan/stor6/dataset/ocr/english_words_50w/english_words_50w_label.txt"
    img_dir = "/tmp/fengcheng/dataset/ocr/english_words_50w"
    save_path = "/tmp/fengcheng/dataset/ocr/english_words_50w/english_words_50w_label.txt"
    generate_label(text_file, img_dir, save_path, rm_space=True)


if __name__ == "__main__":
    # icdar()
    # long_text()
    # qingjie80W()
    # qingjie80W_1()
    # normal()
    # tight_bound()
    # tight_numberletters()
    # tight_english()
    # tight_english_50w()
    # receipt_text()
    # receipt_number()
    # narrow_english_text150w()
    english_50w()
    print(UNKNOW_CHARS)
