# -*- coding: utf-8 -*-
#================================================================
#   God Bless You.
#
#   file name: bin.py
#   author: klaus
#   email: klaus.cheng@qq.com
#   created date: 2019/05/05
#   description:
#
#================================================================

import os
import sys
from functools import partial
from multiprocessing import Pool

CURRENT_FILE_DIRECTORY = os.path.abspath(os.path.dirname(__file__))

from image_writer import ImageWriter
import templates
from templates import PARAM_SETS
from generator import ImageGenerator

SAVE_DIR = os.path.expanduser("~/stor6/dataset/ocr")


def write_set(dirty_arg):
    i, params, num = dirty_arg
    save_dir = os.path.join(SAVE_DIR, params["name"])
    params["save_dir"] = save_dir

    image_generator = ImageGenerator(params)
    image_generator.parall_generate(num, n_process=8) 


def qingjie_800k():
    sets = templates.BASIC
    sets["name"] = "qingjie_80W"
    text_file = os.path.join(CURRENT_FILE_DIRECTORY,
                             "../data/texts/qingjie80W/qingjie80W_0.txt")
    sets["text_file_to_write"] = text_file
    write_set((0, sets, 800000))


def qingjie_800k_1():
    sets = templates.BASIC
    sets["name"] = "qingjie_80W_1"
    text_file = os.path.join(CURRENT_FILE_DIRECTORY,
                             "../data/texts/qingjie80W/qingjie80W_1.txt")
    sets["text_file_to_write"] = text_file
    write_set((0, sets, 800000))


def long_text():
    """ write long texts. text length between 40 and 60.
    """
    sets = templates.BASIC
    sets["name"] = "long_text"
    text_file = os.path.join(CURRENT_FILE_DIRECTORY,
                             "../data/texts/longtext20W.txt")
    sets["text_file_to_write"] = text_file
    write_set((0, sets, 200000))


def icdar_text():
    """ write long texts. text length between 40 and 60.
    """
    sets = templates.BASIC
    sets["name"] = "icdar_like"
    sets["dir_list"] = ["icdar_bg", "complete"]
    text_file = os.path.join(CURRENT_FILE_DIRECTORY,
                             "../data/texts/icdar20W.txt")
    sets["text_file_to_write"] = text_file
    write_set((0, sets, 200000))


def normal():
    sets = PARAM_SETS
    args = []
    for s in sets:
        s["name"] = "normal/" + s["name"]

    text_file_pattern = os.path.join(
        CURRENT_FILE_DIRECTORY, "../data/texts/normal600W/normal600W_{}.txt")
    nums = [400000] * len(sets)
    for i, params in enumerate(sets):
        params["text_file_to_write"] = text_file_pattern.format(i)
        args.append((i, params, nums[i]))

    p = Pool(8)
    p.map(write_set, args)

def tight_bound():
    sets = templates.TIGHT_BOUND
    sets["name"] = "tight_bound"
    text_file = os.path.join(CURRENT_FILE_DIRECTORY,
                             "../data/texts/normal600W/normal600W_14.txt")
    sets["text_file_to_write"] = text_file
    write_set((0, sets, 800000))


def tight_bound_number():
    sets = templates.TIGHT_BOUND
    sets["name"] = "tight_numberletters"
    text_file = os.path.join(CURRENT_FILE_DIRECTORY,
                             "../data/texts/numbers30w.txt")
    sets["text_file_to_write"] = text_file
    write_set((0, sets, 800000))

def tight_bound_english():
    sets = templates.TIGHT_BOUND
    sets["name"] = "tight_english50w"
    text_file = os.path.join(CURRENT_FILE_DIRECTORY,
                             "../data/texts/english_words_50w.txt")
    sets["text_file_to_write"] = text_file
    write_set((0, sets, 800000))

def narrow_text():
    sets = templates.NARROW_TEXT
    sets["name"] = "receipt_text"
    text_file = os.path.join(CURRENT_FILE_DIRECTORY,
                             "../data/texts/receipt50W.txt")
    sets["text_file_to_write"] = text_file
    write_set((0, sets, 800000))

def narrow_receipt_number():
    sets = templates.NARROW_TEXT
    sets["name"] = "receipt_number"
    text_file = os.path.join(CURRENT_FILE_DIRECTORY,
                             "../data/texts/receipt_number30w.txt")
    sets["text_file_to_write"] = text_file
    write_set((0, sets, 800000))

def narrow_english_text():
    sets = templates.NARROW_TEXT
    sets["font_root_dir"] = templates.BASIC["font_root_dir"] 
    sets["name"] = "english150w_with_space"
    text_file = os.path.join(CURRENT_FILE_DIRECTORY,
                             "../data/texts/english_text_150w_1.txt")
    sets["text_file_to_write"] = text_file
    write_set((0, sets, 1500000))

def narrow_english_words():
    sets = templates.BASIC
    sets["crop_policy"] = templates.keys.POLICY_CROP.V_RANDOM 
    sets["x_interval_type"] = templates.keys.INTERVAL.V_X_NARROW 
    sets["font_root_dir"] = templates.BASIC["font_root_dir"] 
    sets["name"] = "english_words_50w"
    text_file = os.path.join(CURRENT_FILE_DIRECTORY,
                             "../data/texts/english_words_50w.txt")
    sets["text_file_to_write"] = text_file
    write_set((0, sets, 500000))

if __name__ == "__main__":
    # qingjie_800k()
    # qingjie_800k()
    # long_text()
    # icdar_text()
    # qingjie_800k_1()
    # normal()
    # tight_bound() 
    # tight_bound_number() 
    # tight_bound_english() 
    # narrow_text() 
    # narrow_receipt_number() 
    # narrow_english_text() 
    narrow_english_words() 
