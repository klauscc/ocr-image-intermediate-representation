# -*- coding: utf-8 -*-
#================================================================
#   God Bless You.
#
#   file name: split_text.py
#   author: klaus
#   email: klaus.cheng@qq.com
#   created date: 2019/05/08
#   description:
#
#================================================================

import os
import argparse
import numpy as np


def arg_parse():
    arg_parser = argparse.ArgumentParser()
    arg_parser.add_argument("--text_file", "-f", help="the txt file to split")
    arg_parser.add_argument("--save_dir",
                            "-s",
                            help="directory to save the files")
    return arg_parser


args = arg_parse().parse_args()
text_file = args.text_file

each_part_n = 400000
save_dir = save_dir = args.save_dir

os.system("mkdir -p {}".format(save_dir))

with open(text_file, "r") as f:
    lines = f.read().splitlines()

n = len(lines)
np.random.shuffle(lines)
i = 0
cur = 0
while n > 0:
    text_filename = os.path.split(text_file)[1]
    part_save_path = os.path.join(
        save_dir, text_filename.replace(".txt", "_{}.txt".format(i)))
    with open(part_save_path, "w") as f:
        for line in lines[cur:cur + each_part_n]:
            f.write(line + "\n")
    i += 1
    cur += each_part_n
    n -= each_part_n
