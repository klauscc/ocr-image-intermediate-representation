# -*- coding: utf-8 -*-
#================================================================
#   God Bless You.
#
#   file name: convert_vocab.py
#   author: klaus
#   email: klaus.cheng@qq.com
#   created date: 2019/05/10
#   description:
#
#================================================================

import os

CURRENT_FILE_DIRECTORY = os.path.abspath(os.path.dirname(__file__))

vocab_file = os.path.join(CURRENT_FILE_DIRECTORY,
                          "../../charset/clean_charset_4pd.txt")

save_file = os.path.join(CURRENT_FILE_DIRECTORY,
                         "../../charset/charset_4pd_list.txt")

with open(vocab_file, "r") as rf, open(save_file, "w") as wf:
    chars = rf.read().splitlines()[0]
    chars = sorted(list(chars))
    for char in chars:
        wf.write(char + "\n")
