# -*- coding: utf-8 -*-
#================================================================
#   God Bless You.
#
#   file name: split_valval.py
#   author: klaus
#   email: klaus.cheng@qq.com
#   created date: 2019/05/13
#   description:
#
#================================================================

import os
import numpy as np

CURRENT_FILE_DIRECTORY = os.path.abspath(os.path.dirname(__file__))

label_file = os.path.join(os.path.expanduser( "~/datasets") ,
                          "cf_generated/")

train_file = label_file.replace("image_label.txt", "train.txt")
val_file = label_file.replace("image_label.txt", "val.txt")

with open(label_file, "r") as f:
    lines = f.read().splitlines()

with open(train_file, "w") as tf, open(val_file, "w") as vf:
    for i, line in enumerate(lines):
        if i % 10 == 0:
            vf.write("{}\n".format(line))
        else:
            tf.write("{}\n".format(line))
