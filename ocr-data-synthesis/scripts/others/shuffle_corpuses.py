# -*- coding: utf-8 -*-
#================================================================
#   God Bless You.
#
#   file name: shuffle_copurses.py
#   author: klaus
#   email: klaus.cheng@qq.com
#   created date: 2019/07/02
#   description:
#
#================================================================

import os
import glob
import numpy as np
CURRENT_FILE_DIRECTORY = os.path.abspath(os.path.dirname(__file__))

base_dir = os.path.join(CURRENT_FILE_DIRECTORY, "../../data/material/*.txt")

corpus_files = glob.glob(base_dir)

for corpus in corpus_files:
    lines = open(corpus).read().splitlines()   
    np.random.shuffle(lines) 
    with open(corpus, "w") as f:
        for line in lines:
            f.write(line+"\n") 
