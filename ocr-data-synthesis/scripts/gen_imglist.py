# -*- coding: utf-8 -*-
#================================================================
#   God Bless You.
#
#   file name: gen_imglist.py
#   author: klaus
#   email: klaus.cheng@qq.com
#   created date: 2019/05/14
#   description:
#
#================================================================

import os
import numpy as np
import string

CURRENT_FILE_DIRECTORY = os.path.abspath(os.path.dirname(__file__))

MATEARIAL_DIR = os.path.join(CURRENT_FILE_DIRECTORY, "../data/material")


def generate_texts(num,
                   save_path,
                   img_prefix,
                   min_len,
                   max_len,
                   random_start=True,
                   filenames=None,
                   post_process_fn=None):
    """
    Args:
        num: Number images to generate
        save_path: Path to save the generated imglist.
    """

    if filenames is None:
        filenames = ["news_sohusite.txt", "news_tensite.txt"]
    n = 0
    p = 0.05
    texts = []
    finished = False
    for i in range(5):
        filenames.extend(filenames)
    for text_file in filenames:
        file_path = os.path.join(MATEARIAL_DIR, text_file)
        if finished:
            break
        for line in open(file_path, "r"):
            line = line.rstrip()
            if np.random.rand() < p:    # pick
                length = len(line)
                if len(line) <= min_len:
                    continue
                if random_start:
                    start_idx = np.random.randint(0, length - min_len)
                else:
                    start_idx = 0
                pick_text_len = np.random.randint(min_len, max_len)
                label = line[start_idx:start_idx + pick_text_len]
                label = label.replace(" ", "")
                label = label.replace("　", "")
                if post_process_fn:
                    label = post_process_fn(label)
                if label == "":
                    continue
                texts.append(label)
                n += 1
                if n % 10000 == 0:
                    print("already picked: {}".format(n))
                if n == num:
                    finished = True
                    break
    with open(save_path, "w") as f:
        for i, line in enumerate(texts):
            write_line = "{}_{:08}.jpg {}\n".format(img_prefix, i, line)
            f.write(write_line)


def long_text():
    num = 200000
    save_path = os.path.join(CURRENT_FILE_DIRECTORY, "../data/texts/longtext20W.txt")
    generate_texts(num, save_path, img_prefix="long_text", min_len=40, max_len=60)


def icdar_text():
    num = 200000
    save_path = os.path.join(CURRENT_FILE_DIRECTORY, "../data/texts/icdar20W.txt")
    generate_texts(num, save_path, img_prefix="icdar_like", min_len=2, max_len=10)


def normal_text_big():
    num = 6000000
    save_path = os.path.join(CURRENT_FILE_DIRECTORY, "../data/texts/normal600W.txt")
    generate_texts(num, save_path, img_prefix="normal", min_len=1, max_len=40)


def normal_text():
    num = 2000000
    save_path = os.path.join(CURRENT_FILE_DIRECTORY, "../data/texts/normal200W.txt")
    generate_texts(num, save_path, img_prefix="normal", min_len=1, max_len=40)


def normal_english_text():
    num = 10000
    save_path = os.path.expanduser("~/stor6/dataset/ocr_dataset/english")
    generate_texts(num, save_path, img_prefix="english", min_len=1, max_len=15)


def receipt_text():
    num = 500000
    save_path = os.path.join(CURRENT_FILE_DIRECTORY, "../data/texts/receipt50W.txt")

    def post_process(label):
        if np.random.rand() < 0.5:
            return label
        else:
            digit_len = np.random.randint(6, 22)
            digits = "".join([str(np.random.randint(0, 10)) for i in range(digit_len)])
            return label + digits

    filenames = [os.path.join(CURRENT_FILE_DIRECTORY, "../data/material/address.txt")]
    generate_texts(num,
                   save_path,
                   img_prefix="receipt",
                   min_len=8,
                   max_len=30,
                   random_start=False,
                   filenames=filenames,
                   post_process_fn=post_process)


def receipt_number():
    num = 300000
    save_path = os.path.join(CURRENT_FILE_DIRECTORY, "../data/texts/receipt_number30w.txt")

    def post_process(label):
        digit_len = np.random.randint(6, 22)
        digits = "".join([str(np.random.randint(0, 10)) for i in range(digit_len)])
        if np.random.rand() < 0.5:
            digits += np.random.choice(list(string.ascii_uppercase))
        return label + digits

    filenames = [os.path.join(CURRENT_FILE_DIRECTORY, "../data/material/address.txt")]
    generate_texts(num,
                   save_path,
                   img_prefix="receipt_number",
                   min_len=0,
                   max_len=1,
                   random_start=False,
                   filenames=filenames,
                   post_process_fn=post_process)


if __name__ == "__main__":
    # long_text()
    # icdar_text()
    # normal_text()
    # normal_text_big()
    # receipt_text()
    receipt_number()
