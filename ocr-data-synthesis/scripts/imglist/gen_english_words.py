# -*- coding: utf-8 -*-
#================================================================
#   God Bless You.
#
#   file name: gen_english_words.py
#   author: klaus
#   email: klaus.cheng@qq.com
#   created date: 2019/06/11
#   description:
#
#================================================================

import os
import sys
import numpy as np
import glob
import html
import xml.etree.ElementTree as ET
from multiprocessing_generator import ParallelGenerator


CURRENT_FILE_DIRECTORY = os.path.abspath(os.path.dirname(__file__))


def load_texts():
    blog_dir = os.path.join(CURRENT_FILE_DIRECTORY, "../../data/material/blogs")
    files = glob.glob(blog_dir + "/*.xml")
    np.random.shuffle(files)
    posts = []
    for i in range(100):
        try:
            file_posts = ET.parse(files[i]).getroot().findall("post")
            for post in file_posts:
                posts.append(html.unescape(post.text.strip().replace("\n", "")).split(" "))
        except Exception as e:
            print(files[i], e)
    return posts


def gen_single_label(posts, max_words):
    post = np.random.choice(posts)
    while len(post) < max_words + 3:
        post = np.random.choice(posts)
    if np.random.rand() < 0.8:
        if np.random.rand() < 0.7: 
            word_count = np.random.randint(1, 10)
        else:
            word_count = np.random.randint(10,max_words+1) 
    else:
        word_count = 1
    idx = np.random.randint(0, len(post) - word_count)
    label = " ".join(post[idx:idx + word_count])
    if label.strip() == "":
        label = gen_single_label(posts, max_words)
    return label

def generator(max_words):
    posts = load_texts()
    while True:
        label = gen_single_label(posts, max_words)
        yield label

def gen_words(num, save_path, max_words=3):
    posts = load_texts()
    name = "english_text_{:07d}.jpg"
    with open(save_path, "w") as f:
        with ParallelGenerator(generator(max_words), max_lookahead=100 ) as g:
            for i,label in enumerate(g):
                line = name.format(i) + " " + label + "\n"
                f.write(line)
                if i % 5000 == 0:
                    print("generated image count: ", i)
                if i == num:
                    break


if __name__ == "__main__":
    save_path = sys.argv[1]
    max_words = 20
    gen_words(1500000, save_path, max_words)
