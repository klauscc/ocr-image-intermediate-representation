# -*- coding: utf-8 -*-
#================================================================
#   God Bless You.
#
#   file name: augmenter.py
#   author: klaus
#   email: klaus.cheng@qq.com
#   created date: 2019/05/05
#   description:
#
#================================================================

import time
import cv2
import numpy as np
import skimage
from skimage.transform import rotate
import imgaug.augmenters as iaa
from PIL import Image, ImageEnhance
import keys


def random_aug_fn(fn, im, delta_range):
    v = np.random.uniform(delta_range[0], delta_range[1])
    return fn(im, v)


RAND_FN = np.random.rand
sometimes = lambda aug: iaa.Sometimes(0.5, aug)

rand_scalar = lambda scale: scale * (np.random.rand() - 0.5) * 2


class PerspectiveTransform(object):
    """perform perspective transform"""

    def __init__(self, params):
        """TODO: to be defined1. """
        self.params = params

    @staticmethod
    def min_rectangle(box):
        t = np.min(box[:, 1])
        l = np.min(box[:, 0])
        r = np.max(box[:, 0])
        b = np.max(box[:, 1])
        return np.array([[l, t], [r, t], [r, b], [l, b]])

    def transform(self, img, box):
        """TODO: Docstring for transform.

        Args:
            img (TODO): TODO
            box (TODO): TODO

        Returns: TODO

        """
        if np.random.rand() < 0.5:
            return self._parallelogram_transform(img, box)
        else:
            return self._random_transform(img, box)

    def _random_transform(self, img, box):
        box = np.array(box, dtype="float32")
        t = np.min(box[:, 1])
        l = np.min(box[:, 0])
        r = np.max(box[:, 0])
        b = np.max(box[:, 1])
        h = b - t
        w = r - l
        jitter_fn = lambda scale: scale * (np.random.rand(4) - 0.5) * 2
        jitter_y = h * jitter_fn(0.2)
        jitter_x = h * jitter_fn(0.5)

        new_box = box.copy()
        new_box[:, 0] = box[:, 0] - jitter_x
        new_box[:, 1] = box[:, 1] - jitter_y
        M = cv2.getPerspectiveTransform(box, new_box)
        wrapped_img = cv2.warpPerspective(img, M, (img.shape[1], img.shape[0]))
        return wrapped_img, new_box

    def _parallelogram_transform(self, img, box):
        box = np.array(box, dtype="float32")
        t = np.min(box[:, 1])
        l = np.min(box[:, 0])
        r = np.max(box[:, 0])
        b = np.max(box[:, 1])
        h = b - t
        w = r - l
        shift_y = h * rand_scalar(0.5)
        shift_y_narrow = h * rand_scalar(0.1)
        change_left_y = np.random.rand() < 0.5
        idx = [0, 3] if change_left_y else [1, 2]
        new_box = box.copy()
        new_box[idx, 1] += shift_y
        new_box[idx[1], 1] += shift_y_narrow

        shift_x = [h * rand_scalar(0.1) for i in range(4)]
        new_box[:, 0] += shift_x

        M = cv2.getPerspectiveTransform(box, new_box)
        wrapped_img = cv2.warpPerspective(img, M, (img.shape[1], img.shape[0]))
        return wrapped_img, new_box


class ImageAugmenter(object):
    """Docstring for ImageAugmenter. """

    def __init__(self, params):
        """TODO: to be defined1. """
        self.params = params
        self.seq = iaa.Sequential(
            [
        # iaa.PerspectiveTransform(scale=(0.01, 0.04)),
                sometimes(iaa.GaussianBlur(sigma=(0.0, 0.5))),
        # sometimes(iaa.ElasticTransformation(alpha=(0.5, 1.5), sigma=0.25))
            ],
            random_order=True)

    def augment(self, image, colors=None):
        """Only perfrom augmentations such as illumination, quality and etc. Geometric 
        augumentations are done on the text.

        Args:
            image: np.ndarray. Image with dim 3: [H, W, 3]

        Returns: augmentated image.

        """
        params = self.params
        h, w, _ = image.shape

        if RAND_FN() < params["no_aug_prob"]:
            return image

        image = self.seq(images=[image])[0]

        if RAND_FN() < params["change_quality_prob"]:
            image = random_aug_fn(self.change_quality, image, [30, 50])

        if RAND_FN() < params["change_brightness_prob"]:
            image = random_aug_fn(self.change_brightness, image, [0.7, 1.3])

        if RAND_FN() < params["change_illuminant_prob"]:
            image = random_aug_fn(self.change_illuminant, image, [0.3, 0.5])
        """
        if RAND_FN() < params["add_rotate_prob"]:
            angle = 2.0 if float(w) / h < 15 else 1.0
            image = random_aug_fn(self.add_rotate, image, [-angle, angle])

        if RAND_FN() < params["add_erode_prob"]:
            image = random_aug_fn(self.add_erode, image, [1, 3])

        if RAND_FN() < params["add_dilate_prob"]:
            image = random_aug_fn(self.add_dilate, image, [1, 3])
        """

        image = self.image_enhance(image,
                                   params.get("enhance_prob", 0.3),
                                   enhance_factor=[0.6, 1.4])

        if RAND_FN() < params["change_resolution_prob"]:
            low = min(15.0 / h, 0.7)
            image = random_aug_fn(self.change_resolution_ratio, image, [low, 0.9])

        if RAND_FN() < params["add_gaussian_noise_prob"]:
            image = random_aug_fn(self.add_gaussian_noise, image, [0.1, 0.3])

        return image

    @staticmethod
    def add_gaussian_noise(im, beta=0.25):
        original_type = im.dtype
        im = im.astype(np.float32)
        row, col, _ = im.shape
        gaussian = np.random.normal(size=(row, col, 3)).astype(np.float32)
        gaussian_img = cv2.addWeighted(im, 1.0, gaussian, beta, 0)
        return gaussian_img.astype(original_type)

    @staticmethod
    def add_erode(img, size=3):
        size = int(size)
        kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (size, size))
        img = cv2.erode(img, kernel)
        return img

    @staticmethod
    def add_dilate(img, size=3):
        size = int(size)
        kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (size, size))
        img = cv2.dilate(img, kernel)
        return img

    @staticmethod
    def add_rotate(img, angle):
        img = img.astype(np.uint8)
        img = rotate(img, angle, resize=False, mode='wrap')
        img = skimage.img_as_ubyte(img)
        return img

    @staticmethod
    def image_enhance(im: np.ndarray, enhance_prob: float, enhance_factor=[0.5, 1.5]):
        pil_img = Image.fromarray(im)
        Enhance_seq = [
            ImageEnhance.Color, ImageEnhance.Contrast, ImageEnhance.Sharpness,
            ImageEnhance.Brightness
        ]
        np.random.shuffle(Enhance_seq)
        for Enhance in Enhance_seq:
            if RAND_FN() < enhance_prob:
                factor = np.random.uniform(enhance_factor[0], enhance_factor[1])
                pil_img = Enhance(pil_img).enhance(factor)
        return np.asarray(pil_img)

    @staticmethod
    def change_quality(im, quality=10):
        im_en = cv2.imencode('.jpg', im, [int(cv2.IMWRITE_JPEG_QUALITY), quality])[1]
        im_de = cv2.imdecode(im_en, cv2.IMREAD_COLOR)
        return im_de

    @staticmethod
    def change_resolution_ratio(im, ratio=0.5):
        h, w = im.shape[:2]
        im_resize = cv2.resize(im, (int(ratio * w), int(ratio * h)), interpolation=cv2.INTER_CUBIC)
        im_resize = cv2.resize(im_resize, (w, h), interpolation=cv2.INTER_CUBIC)
        return im_resize

    @staticmethod
    def change_brightness(im: np.ndarray, percetage=0.7):
        im_brightness = im
        im_brightness[:, :, 0] = np.clip(np.int0(im[:, :, 0] * percetage), a_max=255, a_min=0)
        im_brightness[:, :, 1] = np.clip(np.int0(im[:, :, 1] * percetage), a_max=255, a_min=0)
        im_brightness[:, :, 2] = np.clip(np.int0(im[:, :, 2] * percetage), a_max=255, a_min=0)
        return im_brightness

    @staticmethod
    def change_illuminant(im, ratio=0.5):
        h, w = im.shape[:2]
        center_x = np.random.randint(0, w)
        center_y = np.random.randint(0, h)
        max_d = np.sqrt(
            np.square(max(w - center_x, center_x)) + np.square(max(h - center_y, center_y)))

        ori_dtype = im.dtype

        iv, jv = np.meshgrid(np.arange(h), np.arange(w), indexing="ij")
        iv = iv.astype(np.float32)
        jv = jv.astype(np.float32)
        dx = np.square(jv - center_x)
        dy = np.square(iv - center_y)
        d = np.sqrt(dx + dy)
        percentage = d * -1.0 * ratio / max_d + 1.0
        percentage = np.expand_dims(percentage, -1)
        percentage = np.concatenate([percentage for i in range(im.shape[-1])], axis=-1)

        im_light = im.astype(np.float32)
        im_light = im_light * percentage
        im_light = np.clip(im_light, 0, 255)
        return im_light.astype(ori_dtype)


class TextAugmenter(object):
    """Augmenter for text pil image. The main purpose is to add random erosion to the Texts
    The augmented image must be PIL IMAGE with "RGBA" type.

    """

    def __init__(self, params):
        """TODO: to be defined1. """
        self.params = params

        self.perspective_transform = PerspectiveTransform(params)

    def augment(self, img, box=None):
        """augment image

        Args:
            img: PIL.Image. The image must with "RGBA" format.
            box: List. The bounding box points: [top_left, top_right, bottom_right, bottom_left]. Each point is of (x,y)

        Returns: augmented pil image.
        """
        params = self.params
        text_erode_type = params.get("text_erode_type", "random_all")
        do_erode_prob = params.get("do_erode_prob", 0.4)
        text_erode_max_prob = max(0.0001, params.get("text_erode_max_prob", 0.2))
        erode_prob = np.random.uniform(0, text_erode_max_prob)

        if RAND_FN() < 0.4 and box is not None:
            img = np.array(img)
            img, new_box = self.perspective_transform.transform(img, box)
            img = Image.fromarray(img)
            box = self.perspective_transform.min_rectangle(new_box)

        if RAND_FN() > do_erode_prob:
            img = img
        else:    # else erode text
            if text_erode_type == "random":
                img = self.randomly_erode(img, erode_prob)
            elif text_erode_type == "line":
                img = self.line_erode(img, erode_prob)
            elif text_erode_type == "random_all":
                erode_fn = [self.randomly_erode, self.line_erode]
                ith = np.random.randint(len(erode_fn))
                img = erode_fn[ith](img, erode_prob)
            else:
                img = img

        if box is not None:
            return img, box
        else:
            return img

    @staticmethod
    def randomly_erode(pil_im, erode_prob):
        w, h = pil_im.size
        mask = np.ones((h, w), dtype=int)
        mask = mask * (np.random.rand(h, w) < erode_prob)
        mask = mask.astype(np.bool)

        text_np = np.array(pil_im)
        shaded_alpha = np.random.randint(0, 32)
        text_np[mask, 3] = shaded_alpha
        return Image.fromarray(text_np)

    @staticmethod
    def random_shift(pil_im, shift_prob):
        w, h = pil_im.size
        # shift_h = j

        np_im = np.array(pil_im)

    @staticmethod
    def line_erode(pil_im, erode_prob):
        w, h = pil_im.size
        vertical_line = int(w * 0.5 * erode_prob)
        horizontal_line = int(h * 0.5 * erode_prob)

        text_np = np.array(pil_im)
        horizontal_line_ys = np.random.randint(0, h, size=horizontal_line)
        vertical_line_xs = np.random.randint(0, w, size=vertical_line)
        for y in horizontal_line_ys:
            if np.sum(text_np[y, :, 3]) == 0:
                continue
            else:
                text_np[y, :, 3] = 0
        for x in vertical_line_xs:
            if np.sum(text_np[:, x, 3]) == 0:
                continue
            else:
                text_np[:, x, 3] = 0
        return Image.fromarray(text_np)


class Cropper(object):
    """Docstring for Cropper. """

    def __init__(self, params):
        """TODO: to be defined1.

        Args:
            params: dict (TODO): TODO


        """
        self.params = params

    def get_box_jitter(self, h):
        """jitter the bounding box
        Args:
            h: Int. The height of the reference bounding box.
            
        Returns: List of 4 int32. The offsets in [u, b, l, r]. 

        """
        ratios = np.random.rand(4) - 0.3    #[-0.3,0.7]
        base_offsets = [-0.3 * h, 0.4 * h, -0.5 * h, 0.5 * h]
        offsets = [0, 0, 0, 0]
        if self.params[keys.POLICY_CROP.KEY] == keys.POLICY_CROP.V_RANDOM:
            rand_p = np.random.rand()
            if rand_p < 0.3:    # left and right have long margin
                p1 = np.random.rand()
                if p1 < 0.33:
                    offsets = [-0.1 * h, 0.2 * h, -2 * h, 2 * h]
                elif p1 < 0.66:
                    offsets = [-0.1 * h, 0.2 * h, -2 * h, 0.2 * h]
                else:
                    offsets = [-0.1 * h, 0.2 * h, -0.2 * h, 2 * h]
            elif rand_p < 0.7:
                offsets = [-0.2 * h, 0.2 * h, 0.2 * h, 0.2 * h]
            else:
                offsets = [0, 0, 0, 0]
            ratios *= 0.8
            offsets += base_offsets * ratios
        elif self.params[keys.POLICY_CROP.KEY] == keys.POLICY_CROP.V_TIGHT:
            ratios *= 0.8    # [-0.24, 0.56]
            offsets = base_offsets * ratios
        else:
            offsets = [0, 0, 0, 0]
        return np.array(offsets).astype(np.int32)

    def get_min_rect_box(self, box):
        """get the minimum bounding box.

        Args:
            box: List. The bounding box points: [top_left, top_right, bottom_right, bottom_left]. 
                Each point is of (x,y).

        Returns: List. 
                The box is a 4 element int32 List [upper_y, bottom_y, left_x, right_x].

        """
        box = np.array(box)
        return [np.min(box[:, 1]), np.max(box[:, 1]), np.min(box[:, 0]), np.max(box[:, 0])]

    def crop(self, image, box, offsets=None):
        """TODO: Docstring for crop.

        Args:
            image: np.ndarray. Image with dim 3: [H, W, 3]
            box: List. If rectangle is False, the bounding box points: [top_left, top_right, bottom_right, bottom_left]. 
                Each point is of (x,y). Otherwise, the box is a 4-elment int list [upper_y, bottom_y, left_x, right_x] 
                randomized by func `get_crop_box` 
            rectangle: Boolean. Whether the box is a rectangle.
            offsets: Int List of 4 element. Represent the offsets of [u,b,l,r] 

        Returns: The cropped image.

        """
        img_h, img_w = image.shape[:2]
        box = np.array(box)
        if offsets is not None:
            box += offsets
        u, b, l, r = np.array(box).astype(np.int32)
        l = np.clip(l, 0, img_w)
        r = np.clip(r, l + 32, img_w)
        u = np.clip(u, 0, img_h)
        b = np.clip(b, u + 10, img_h)
        return image[u:b, l:r]
