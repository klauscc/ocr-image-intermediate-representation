#coding=utf-8
#python3
import os
import cv2
import numpy as np
from PIL import Image, ImageDraw, ImageFont
from skimage.transform import rotate
import skimage
import typing

from colormath.color_objects import AdobeRGBColor, LabColor
from colormath.color_diff import delta_e_cie2000
from colormath.color_conversions import convert_color

from utils.common import truncx


class FontGenerator(object):
    def __init__(self, params):
        root_dir = params["font_root_dir"]
        font_size_range = params["font_size_range"]
        self.font_size_range = font_size_range
        self.font_size_list = list(
            range(font_size_range[0], font_size_range[1]))

        self.params = params

        self.size_font_dict, self.charset_list = self.load_font(root_dir)

        self.font_num = len(self.size_font_dict[self.font_size_list[0]])
        print("font_num: {}".format(self.font_num))

    def font_num(self):
        return self.font_num

    def get_font(self, texts, font_choose_policy):

        use_same_font = True
        use_same_font_size = True
        if font_choose_policy == "random_font_and_size" or font_choose_policy == "random":
            use_same_font = False
            use_same_font_size = False
        elif font_choose_policy == "random_font":
            use_same_font = False
        elif font_choose_policy == "random_font_size":
            use_same_font_size = False

        def pick_font_size(seed):
            font_size_randomstate = np.random.RandomState(seed=seed)
            return font_size_randomstate.choice(self.font_size_list)

        def pick_font(font_size, char, seed):
            font_randomstate = np.random.RandomState(seed=seed)
            font_idx = font_randomstate.randint(0, self.font_num)
            i = 0
            while not self.check_font_valid(self.charset_list[font_idx], char):
                i += 1
                font_idx = font_randomstate.randint(0, self.font_num)
                if i >= 500:
                    raise Exception(
                        "tried 100 times. no font found for text: {}".format(
                            char))
            return self.size_font_dict[font_size][font_idx]

        fonts = []
        base_font_size = np.random.choice(self.font_size_list)
        for text in texts:
            font_choice_seed = np.random.randint(
                0, 1000000) if use_same_font else None
            font_size_choice_seed = np.random.randint(
                0, 1000000) if use_same_font_size else None
            font_size_fn = lambda: np.random.randint(
                max(base_font_size - 3, self.font_size_range[0]),
                min(base_font_size + 3, self.font_size_range[1]))
            if use_same_font_size:
                group_base_font_size = font_size_fn()
                font_sizes = [group_base_font_size] * len(text)
            else:
                font_sizes = [font_size_fn() for i in range(len(text))]

            for i, char in enumerate(text):
                font_size = pick_font_size(font_size_choice_seed)
                font = pick_font(font_sizes[i], char, font_choice_seed)
                fonts.append(font)
        return fonts

    def check_font_valid(self, charset, text):
        for t in text:
            if t not in charset:
                return False
        return True

    def load_font(self, root_dir):
        size_font_dict = {}
        ttf_dir = os.path.join(root_dir, 'ttf')
        txt_dir = os.path.join(root_dir, 'txt')
        charset_list = []
        for i, fs in enumerate(self.font_size_list):
            font_list = []
            for root, dirs, files in os.walk(ttf_dir):
                for filename in files:
                    suffix = os.path.splitext(filename)[1]
                    if suffix == '.ttf' or suffix == '.TTF' or suffix == '.otf' or suffix == '.ttc':
                        name = os.path.splitext(filename)[0]
                        txt_name = os.path.join(txt_dir, name + '.txt')
                        font_list.append(
                            ImageFont.truetype(os.path.join(ttf_dir, filename),
                                               fs, 0))
                        if i == 0:
                            with open(txt_name) as f_charset:
                                charset = f_charset.read()
                                charset_list.append(charset)

            size_font_dict[fs] = font_list
        return size_font_dict, charset_list


class BackgroundGenerator():
    def __init__(self, params):
        root_dir = params["bg_root_dir"]
        dir_list = params["dir_list"]
        self.bg_list = self.load_bg(root_dir, dir_list)
        self.bg_size = len(self.bg_list)

    def get_bg(self, text_write_hw, x_intervals, y_intervals,
               bg_choose_policy):

        total_text_width = np.sum(text_write_hw[:, 1])
        text_max_height = max(text_write_hw[:, 0])

        text_x_offset = np.sum(x_intervals)

        text_width = total_text_width + text_x_offset
        text_height = text_max_height * 1.2

        index = np.random.randint(0, self.bg_size)

        bg_img = self.bg_list[index]
        bg_h, bg_w = bg_img.shape[:2]
        height = text_height * 1.5
        width = text_width * 1.5
        h = int(max(height, bg_h))
        w = int(max(width, bg_w))
        return np.pad(bg_img, [(h - bg_h, 0), (w - bg_w, 0), (0, 0)], "wrap")
        # return cv2.resize(bg_img, (w, h))

    def load_bg(self, root_dir, dir_list):
        bg_list = []
        for d in dir_list:
            path = os.path.join(root_dir, d)
            for root, dirs, files in os.walk(path):
                for filename in files:
                    ext = os.path.splitext(filename)[1]
                    if ext == '.jpg' or ext == ".JPEG":
                        image = cv2.imread(os.path.join(path, filename))
                        if image is not None:
                            image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
                            bg_list.append(image)
        return bg_list


class ColorGenerator():
    def __init__(self, params):
        self.black = (0, 0, 0)
        self.blue = (255, 0, 0)
        self.red = (0, 0, 255)
        self.white = (255, 255, 255)

    def get_color(self, texts, background, color_choose_policy):
        rgb_mean = np.mean(background, axis=(0, 1))
        rgb_mean = list(map(int, rgb_mean))

        bg_r, bg_g, bg_b = rgb_mean
        bg_rgb_color = AdobeRGBColor(bg_r, bg_g, bg_b, is_upscaled=True)
        bg_lab_color = convert_color(bg_rgb_color, LabColor)

        def pick_color():
            rand_color_fn = lambda: np.random.randint(0, 256, size=3)
            color = rand_color_fn()
            i = 0
            while True:
                if i == 10:
                    break
                r, g, b = color
                rgb_color = AdobeRGBColor(r, g, b, is_upscaled=True)
                lab_color = convert_color(rgb_color, LabColor)
                if delta_e_cie2000(bg_lab_color, lab_color) < 50:
                    color = rand_color_fn()
                    i += 1
                else:
                    break

            return tuple(color)

        use_same_color = True
        if color_choose_policy.lower() == "random":
            use_same_color = False

        colors = []
        for part in texts:
            if use_same_color:
                base_color = pick_color()
                part_color = [base_color] * len(part)
            else:
                part_color = []
                for i in range(len(part)):
                    color = tuple(pick_color())
                    part_color.append(color)
            colors.extend(part_color)
        return colors


class TextGenerator():
    def __init__(self, params):
        filename = params["text_file_to_write"]
        self.name_text_list = self.load_text(filename)
        self.list_size = len(self.name_text_list)
        self.cnt = 0

    def get_text(self):
        if self.cnt < self.list_size:
            name_text = self.name_text_list[self.cnt]
            self.cnt += 1
            return self.cnt - 1, name_text.split(" ", 1)
        else:
            return -1, ['', '']

    def load_text(self, filename):
        f_read = open(filename, 'r')
        line = f_read.readline()
        name_text_list = []
        while line:
            name_text_list.append(line[0:len(line) - 1])
            line = f_read.readline()
        f_read.close()
        # name_text_list = sorted(name_text_list, key=lambda c: len(c))
        np.random.shuffle(name_text_list)
        return name_text_list
