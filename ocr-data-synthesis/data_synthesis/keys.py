# -*- coding: utf-8 -*-
#================================================================
#   God Bless You.
#
#   file name: keys.py
#   author: klaus
#   email: klaus.cheng@qq.com
#   created date: 2019/05/07
#   description:
#
#================================================================

KEY_DIVIDE_MAX_N_GROUPS = "divide_max_nparts"
KEY_BG_ROOT_DIR = "bg_root_dir"
KEY_BG_DIR_LIST = "dir_list"
KEY_FONT_ROOT_DIR = "font_root_dir"
KEY_FONT_SIZE_RANGE = "font_size_range"


class INTERVAL:
    KEY_X = "x_interval_type"
    KEY_Y = "y_interval_type"

    V_X_SAME = "same"
    V_X_SAME_WITH_GAP = "same_with_gap"
    V_X_RANDOM = "random"
    V_X_NONE = "none"
    V_X_NARROW = "narrow"

    V_Y_RANDOM = "random"
    V_Y_NONE = "none"


class POLICY_FONT:
    KEY = "font_choose_policy"

    V_GROUP_SAME = "same"
    V_RANDOM = "random"
    V_RANDOM_FONT = "random_font"
    V_RANDOM_FONT_SIZE = "random_font_size"


class POLICY_COLOR:
    KEY = "color_choose_policy"

    V_RANDOM = "random"
    V_GROUP_SAME = "group_same"


class POLICY_BACKGROUND:
    KEY = "bg_choose_policy"

    V_RANDOM = "random"

class POLICY_CROP:
    KEY = "crop_policy"

    V_RANDOM = "random"
    V_TIGHT = "tight"
