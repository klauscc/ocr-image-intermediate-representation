# -*- coding: utf-8 -*-
#================================================================
#   God Bless You.
#
#   file name: templates.py
#   author: klaus
#   email: klaus.cheng@qq.com
#   created date: 2019/05/07
#   description:
#
#================================================================

import os
import sys

CURRENT_FILE_DIRECTORY = os.path.abspath(os.path.dirname(__file__))
sys.path.append(os.path.join(CURRENT_FILE_DIRECTORY, ''))

import keys

BASIC = {
    "name": "basic",

    # bg
    "bg_root_dir": os.path.join(CURRENT_FILE_DIRECTORY, "../data/background"),
    "dir_list": ["complete", "piece"],
    #font
    "font_root_dir": os.path.join(CURRENT_FILE_DIRECTORY, "../data/fonts"),
    "font_size_range": [20, 40],

    # text
    "text_file_to_write": os.path.join(CURRENT_FILE_DIRECTORY, "./texts/fn_text600W.txt"),
    "divide_max_nparts": 1,    # -1, 3
    # x intervals
    "x_interval_type": keys.INTERVAL.V_X_NONE,
    #font
    "font_choose_policy": keys.POLICY_FONT.V_GROUP_SAME,
    #color
    "color_choose_policy": keys.POLICY_COLOR.V_GROUP_SAME,
    #crop
    "crop_policy": keys.POLICY_CROP.V_RANDOM,

    #image augment
    "no_aug_prob": 0.2,
    "change_quality_prob": 0.6,
    "change_brightness_prob": 0.6,
    "change_illuminant_prob": 0.6,
    "add_rotate_prob": 0.4,
    "add_erode_prob": 0.3,
    "add_dilate_prob": 0.3,
    "change_resolution_prob": 0.4,
    "add_gaussian_noise_prob": 0.4,
    "enhance_prob": 0.4,

    #text augment
    "text_erode_type": "random_all",    # random, line, random_all, none
    "do_erode_prob": 0.3,
    "text_erode_max_prob": 0.15,

    #fixed
    #background
    "bg_choose_policy": keys.POLICY_BACKGROUND.V_RANDOM,
    # y intervals
    "y_interval_type": keys.INTERVAL.V_Y_NONE,
}

RANDOM_INTERVAL = BASIC.copy()
RANDOM_INTERVAL.update({
    "x_interval_type": keys.INTERVAL.V_X_RANDOM,
    "name": "random_interval",
})

#random color
RANDOM_COLOR = RANDOM_INTERVAL.copy()
RANDOM_COLOR.update({
    "color_choose_policy": keys.POLICY_COLOR.V_RANDOM,
    "name": "random_color",
})

#random font
RANDOM_FONT = RANDOM_INTERVAL.copy()
RANDOM_FONT.update({
    "font_choose_policy": keys.POLICY_FONT.V_RANDOM_FONT,
    "name": "random_font",
})

#random size
RANDOM_FONT_SIZE = RANDOM_INTERVAL.copy()
RANDOM_FONT_SIZE.update({
    "name": "random_font_size",
    "font_choose_policy": keys.POLICY_FONT.V_RANDOM_FONT_SIZE
})

#random all
RANDOM_ALL = RANDOM_INTERVAL.copy()
RANDOM_ALL.update({
    "name": "random_all",
    "divide_max_nparts": -1,
    "x_interval_type": keys.INTERVAL.V_X_RANDOM,
    "font_choose_policy": keys.POLICY_FONT.V_RANDOM,
    "color_choose_policy": keys.POLICY_COLOR.V_RANDOM,
})

#idcard like
IDCARD_LIKE = RANDOM_INTERVAL.copy()
IDCARD_LIKE.update({
    "name": "idcard_like",
    "divide_max_nparts": 2,
    "x_interval_type": keys.INTERVAL.V_X_SAME_WITH_GAP,
    "color_choose_policy": keys.POLICY_COLOR.V_GROUP_SAME
})

IMAGENET = BASIC.copy()
IMAGENET.update({
    "name": "basic_imagenet",
    "divide_max_nparts": 1,
    "color_choose_policy": keys.POLICY_COLOR.V_GROUP_SAME,
    "dir_list": ["ILSVRC2012_img_val_part"],
})

TIGHT_BOUND = BASIC.copy()
TIGHT_BOUND.update({
    "name": "tight_bound",
    keys.POLICY_CROP.KEY: keys.POLICY_CROP.V_TIGHT,
})

NARROW_TEXT = TIGHT_BOUND.copy()    # for receipt
NARROW_TEXT.update({
    "name": "narrow_text",
    "x_interval_type": keys.INTERVAL.V_X_NARROW,
    "font_root_dir": os.path.join(CURRENT_FILE_DIRECTORY, "../data/fonts/specific"),
    "change_resolution_prob": 1.0,
    "change_quality_prob": 1.0,
    "text_partition_policy": "divide_chinese_num",
})

SPECIFIC_TIGHT_BOUND = TIGHT_BOUND.copy()
SPECIFIC_TIGHT_BOUND.update({
    "name":
    "specific_tight_bound",
    "font_root_dir":
    os.path.join(CURRENT_FILE_DIRECTORY, "../data/fonts/specific")
})

PARAM_SETS = [
    BASIC, RANDOM_INTERVAL, RANDOM_COLOR, RANDOM_FONT, RANDOM_FONT_SIZE, RANDOM_ALL, IDCARD_LIKE,
    IMAGENET, TIGHT_BOUND
]
