# -*- coding: utf-8 -*-
#================================================================
#   God Bless You.
#
#   file name: generator.py
#   author: klaus
#   email: klaus.cheng@qq.com
#   created date: 2019/05/06
#   description:
#
#================================================================

import os
import time
from functools import partial
import tensorflow as tf
from multiprocessing import Process, Queue

from image_writer import ImageWriter
from text_generator import TextGenerator


class ImageGenerator(object):
    """generate text image for ocr"""

    def __init__(self, params: dict):
        """
        Args:
            params: dict. parameters.

        """
        self.params = params

        # self.text_generator = TextGenerator(params)

        self.save_dir = os.path.join(params["save_dir"], "augmented")
        self.gt_save_dir = os.path.join(params["save_dir"], "gt")
        self.auged_mask_save_dir = os.path.join(params["save_dir"], "augmented_mask")
        self.gt_mask_save_dir = os.path.join(params["save_dir"], "gt_mask")
        os.makedirs(self.save_dir, exist_ok=True)
        os.makedirs(self.gt_save_dir, exist_ok=True)
        os.makedirs(self.auged_mask_save_dir, exist_ok=True)
        os.makedirs(self.gt_mask_save_dir, exist_ok=True)

        self.image_writer = ImageWriter(self.params)

        self.queue = Queue(10000)

    def generator(self, param, queue=None):
        """generator for train.
        Returns: TODO
        """
        self.text_generator = TextGenerator(param)
        image_writer = ImageWriter(param)
        while True:
            num, [filename, text] = text_generator.get_text()
            if text == "":
                continue
            try:
                image = image_writer.write(text, save_path="", save=False)
                queue.put((image, text))
            except Exception as e:
                continue

    def start_generator(self, param, queue, n_worker):
        """TODO: Docstring for start_generator.

        Kwargs:
            param:
            queue:
            n_worker (TODO): TODO

        Returns: TODO

        """
        self.workers = [
            Process(target=self.generator, args=(
                param,
                queue,
            )) for i in range(n_worker)
        ]
        for worker in self.workers:
            worker.daemon = True
            worker.start()

    def consumer(self, queue):
        while True:
            yield queue.get()

    def tf_dataset_parallel(self, n_worker=4):
        self.start_generator(self.queue, n_worker)

        gen = partial(self.consumer, queue=self.queue)
        return tf.data.Dataset.from_generator(gen, (tf.float32, tf.string),
                                              (tf.TensorShape([None, None, 3]), tf.TensorShape([])))

    def generate(self, n=2000000):
        """TODO: Docstring for generate.
        Args:
            n: int. The number of generated images.
        Returns: TODO

        """
        self.text_generator = TextGenerator(self.params)
        for i in range(n):
            num, [filename, text] = self.text_generator.get_text()
            if text == "":
                print(filename, text)
                continue
            text = text.strip()
            if num == -1:
                break
            save_path = os.path.join(self.save_dir, filename)
            try:
                self.image_writer.write(text, save_path)
            except Exception as e:
                print(e)

    def parall_generate(self, n=2000000, n_process=8):
        """TODO: Docstring for parall_generate.

        Kwargs:
            n (TODO): TODO

        Returns: TODO

        """
        self.text_generator = TextGenerator(self.params)
        filename_text_list = []
        for i in range(n):
            num, [filename, text] = self.text_generator.get_text()
            if text == "":
                print(filename, text)
                continue
            text = text.strip()
            filename_text_list.append([filename, text])

        def producer(filename_text_list):
            image_writer = ImageWriter(self.params)
            for filename, text in filename_text_list:
                save_path = os.path.join(self.save_dir, filename)
                gt_save_path = os.path.join(self.gt_save_dir, filename)
                auged_mask_save_path = os.path.join(self.auged_mask_save_dir, filename)
                gt_mask_save_path = os.path.join(self.gt_mask_save_dir, filename)
                try:
                    image_writer.write(text,
                                       save_path,
                                       gt_save_path=gt_save_path,
                                       mask_save_path=auged_mask_save_path,
                                       gt_mask_save_path=gt_mask_save_path)
                except Exception as e:
                    print(e)

        def split(list_, n):
            k, m = divmod(len(list_), n)
            return (list_[i * k + min(i, m):(i + 1) * k + min(i + 1, m)] for i in range(n))

        divided_filename_text_list = list(split(filename_text_list, n_process))

        workers = [
            Process(target=producer, args=(divided_filename_text_list[i],))
            for i in range(n_process)
        ]
        for worker in workers:
            worker.daemon = True
            worker.start()
        for worker in workers:
            worker.join()
