# -*- coding: utf-8 -*-
#================================================================
#   God Bless You.
#
#   file name: image_writer.py
#   author: klaus
#   email: klaus.cheng@qq.com
#   created date: 2019/05/05
#   description:
#
#================================================================

import os
from typing import List
import numpy as np
import cv2
from PIL import Image, ImageDraw, ImageFont

from utils.common import truncx
from augmenter import ImageAugmenter, Cropper, TextAugmenter
from text_generator import FontGenerator, BackgroundGenerator, ColorGenerator, TextGenerator
import templates


class ImageWriter(object):
    """write text on images"""

    def __init__(self, params: dict):
        """
        Args:
            params: Dict. parameters.
        """
        self.params = params

        self.background_generator = BackgroundGenerator(params)
        self.font_generator = FontGenerator(params)
        self.color_generator = ColorGenerator(params)

        self.image_augmenter = ImageAugmenter(params)
        self.text_augmenter = TextAugmenter(params)
        self.cropper = Cropper(params)

    def write(self,
              text: str,
              save_path: str,
              save=True,
              gt_save_path=None,
              mask_save_path=None,
              gt_mask_save_path=None):
        """TODO: Docstring for write_text_to_background.

        Args:
            text: str. The text to write.
            save_path: str. The path to save image.

        Kwargs:
            save: bool. Default is True. If True, the generated image will save to `save_path`,
                otherwise the image will not be saved.
            gt_save_path: str. The path to save the gt image.
            mask_save_path: str. The path to save the augmented image mask.
            gt_mask_save_path: str. The path to save th gt image mask.

        Returns: np.ndarray. The randomly generated RGB image.

        """
        if text == "":
            return
        params = self.params

        # partition text into parts.
        part_texts = self._partition_text(text, max_nparts=params["divide_max_nparts"])

        # get fonts to write
        fonts = self.font_generator.get_font(part_texts,
                                             font_choose_policy=params["font_choose_policy"])

        # calculate the h and w of each char.
        text_write_hw = np.zeros((len(text), 2))
        for i, char in enumerate(text):
            width, height = fonts[i].getsize(char)
            text_write_hw[i] = [height, width]

        # generate the intervals between chars.
        x_intervals = self.get_x_interval_jitter(part_texts,
                                                 text_write_hw,
                                                 type=params["x_interval_type"])
        y_intervals = self.get_y_interval_jitter(part_texts,
                                                 text_write_hw,
                                                 type=params["y_interval_type"])
        gt_x_intervals = x_intervals
        gt_y_intervals = self.get_y_interval_jitter(part_texts,
                                                    text_write_hw,
                                                    type=templates.keys.INTERVAL.V_Y_NONE)

        # get backgrounds according to text, fonts and intervals
        background = self.background_generator.get_bg(
            text_write_hw,
            x_intervals,
            y_intervals,
            bg_choose_policy=self.params["bg_choose_policy"])
        # get colors according to text and background
        colors = self.color_generator.get_color(part_texts,
                                                background,
                                                color_choose_policy=params["color_choose_policy"])

        bg_height, bg_width = background.shape[0:2]
        background_pil = Image.fromarray(background).convert("RGBA")

        orig_text_pil = Image.new("RGBA", background_pil.size, (0, 0, 0, 0))
        gt_text_pil = Image.new("RGBA", background_pil.size, (0, 0, 0, 0))
        draw = ImageDraw.Draw(orig_text_pil)
        gt_draw = ImageDraw.Draw(gt_text_pil)

        # write text
        orig_text_box = self._write_text_to_background(text, draw, background.shape[:2], fonts,
                                                       colors, text_write_hw, x_intervals,
                                                       y_intervals)
        gt_text_box = self._write_text_to_background(text, gt_draw, background.shape[:2], fonts,
                                                     colors, text_write_hw, gt_x_intervals,
                                                     gt_y_intervals)

        # augment text
        auged_text_pil, auged_text_box = self.text_augmenter.augment(orig_text_pil, orig_text_box)

        # obtain mask
        auged_mask = np.array(auged_text_pil)[..., 3]    #alpha channel
        gt_mask = np.array(gt_text_pil)[..., 3]    #alpha channel

        # overlap text to image
        text_image_pil = Image.alpha_composite(background_pil, auged_text_pil)
        text_image_pil = text_image_pil.convert("RGB")
        gt_image_pil = Image.alpha_composite(background_pil, gt_text_pil)
        gt_image_pil = gt_image_pil.convert("RGB")

        # crop the bounding box
        img_np = np.array(text_image_pil)
        gt_img_np = np.array(gt_image_pil)

        auged_text_box = self.cropper.get_min_rect_box(auged_text_box)
        gt_text_box = self.cropper.get_min_rect_box(gt_text_box)
        h = auged_text_box[1] - auged_text_box[0]
        offsets = self.cropper.get_box_jitter(h)
        crop_img = self.cropper.crop(img_np, auged_text_box, offsets=offsets)
        auged_mask = self.cropper.crop(auged_mask, auged_text_box, offsets=offsets)
        gt_crop_img = self.cropper.crop(gt_img_np, gt_text_box, offsets=offsets)
        gt_mask = self.cropper.crop(gt_mask, gt_text_box, offsets=offsets)

        # augment cropped image
        crop_img = self.image_augmenter.augment(crop_img, colors)

        if save:
            Image.fromarray(crop_img).save(save_path)
            if gt_save_path:
                Image.fromarray(gt_crop_img).save(gt_save_path)
            if mask_save_path:
                Image.fromarray(auged_mask).save(mask_save_path)
            if gt_mask_save_path:
                Image.fromarray(gt_mask).save(gt_mask_save_path)
        return crop_img

    def _partition_text(self, text, max_nparts=-1, params=None):
        """divide the text into several parts.

        Args:
            text: str. The text to divide.

        Kwargs:
            nparts: int. n parts. if negative, The text will be randomly divided

        Returns: List. The divided text.
        """
        if isinstance(params, dict) and params.get("text_partition_policy",
                                                   "") == "divide_chinese_num":
            for i in range(len(text) - 1, -1, -1):
                if text[i].isdigit():
                    continue
                else:
                    break
            if len(text) - i > 4:
                return [text[:i + 1], text[i + 1:]]
            else:
                return [text]

        if len(text) == 1:
            return [text]

        if max_nparts <= 0:
            nparts = np.random.randint(1, max(np.ceil(len(text) / 2.0), 3))
        elif max_nparts == 1:
            return [text]
        else:
            nparts = np.random.randint(1, max_nparts + 1)

        part_boundary = [0]
        part_boundary += sorted(np.random.choice(range(1, len(text)), nparts - 1, replace=True))
        part_boundary += [len(text)]

        texts = []
        for i in range(nparts):
            texts.append(text[part_boundary[i]:part_boundary[i + 1]])
        return texts

    def _write_text_to_background(self, text, draw, bg_hw, fonts, colors, text_write_hw,
                                  x_intervals, y_intervals):
        """
        Args:
            text: str. The text to write.
            draw: ImageDraw. The background to draw on.
            bg_hw: tuple of length 2. The height and width of the background.
            fonts: 1-D List. The font to write each character.
            colors: 1-D List. The color to write each character.
            text_write_hw: 2-D np.array. with shape [len(text),2]. The second dim represents the height and width of each character.
            x_intervals: 1-D List. The x-intervals between each characters.
            y_intervals: 1-D List. The y-intervals between each characters.

        """

        bg_height, bg_width = bg_hw

        total_text_width = np.sum(text_write_hw[:, 1])
        text_heights = text_write_hw[:, 0]
        text_widths = text_write_hw[:, 1]
        max_text_width = max(text_widths)
        max_text_height = max(text_heights)

        cur_x = (bg_width - total_text_width - np.sum(x_intervals)) * np.random.rand()

        total_text_y_offset = np.abs(np.sum(y_intervals))
        text_y = (bg_height - max_text_height) * np.random.rand()

        char_x = []
        char_y_u = []
        char_y_b = []

        for i, char in enumerate(text):
            cur_y = text_y + y_intervals[i]

            cur_x = truncx(cur_x, 0, bg_width - text_widths[i])
            cur_y = truncx(cur_y, 0, bg_height - text_heights[i])

            char_x.append(cur_x)
            char_y_u.append(cur_y)
            char_y_b.append(cur_y + text_write_hw[i, 0])

            draw.text((cur_x, cur_y), char, fill=colors[i] + (255,), font=fonts[i])

            if char == " ":
                x_intervals[i] = np.random.randint(0, 3)
            cur_x += text_widths[i] + x_intervals[i]

        l = min(char_x)
        r = max(char_x) + max_text_width
        u = min(char_y_u)
        b = max(char_y_b)
        return [[l, u], [r, u], [r, b], [l, b]]

    def get_x_interval_jitter(self, texts, text_write_hw, type="random"):
        """get the offsets of the word intervals in x.

        Args:
            texts: List. The texts to write on the image.
            text_write_hw: np.ndarray of shape [l, 2]. l is the length of text. 2 represents the [h, w]

        Kwargs:
            type: Str. The interval type. One of "same", "multipart", "random", "multipart_with_gap"

        Returns: List[float]. Length is of the same as text. The interval between each two characters.

        """
        max_text_width = max(text_write_hw[:, 1])
        offset_fn = lambda: np.random.randint(-1 * max_text_width * 0.05, max_text_width * 1.0)
        gap_fn = lambda: int(max_text_width * np.random.uniform(1.5, 3.0))

        n_chars = 0
        for text in texts:
            n_chars += len(text)

        offsets = []
        if "same" in type.lower():
            for part in texts:
                part_offsets = offset_fn()
                offsets.extend([part_offsets] * (len(part) - 1))
                part_interval = gap_fn() if "gap" in type.lower() else offset_fn()
                offsets.append(part_interval)
        elif "random" in type.lower():
            for part in texts:
                offsets.extend([offset_fn() for i in range(len(part) - 1)])
                part_interval = gap_fn() if "gap" in type.lower() else offset_fn()
                offsets.append(part_interval)
        elif "narrow" in type.lower():
            offset = np.random.randint(-2, 1)
            offsets = [offset] * n_chars
        else:
            offsets = [0] * n_chars
        return offsets

    def get_y_interval_jitter(self, texts, text_write_hw, type="random"):
        """get the offsets of the word intervals in x.

        Args:
            texts: List[str]. The text to write on the image.
            text_write_hw: np.ndarray of shape [l, 2]. l is the length of text. 2 represents the [h, w]

        Kwargs:
            type: Str. The interval type. One of "same", "random"

        Returns: List[float]. Length is of the same as text. The interval between each two characters.

        """
        n_chars = 0
        for text in texts:
            n_chars += len(text)

        max_text_height = max(text_write_hw[:, 0])
        offset_fn = lambda: np.random.randint(0, max_text_height * 0.3)

        offsets = []
        if "random" in type.lower():
            for i in range(n_chars):
                offset = int(max_text_height * 0.03 * np.random.normal(0.0, 1.0))
                offset = np.clip(offset, -max_text_height, max_text_height)
                offsets.append(offset)
        else:
            offsets = [0] * n_chars

        return offsets
