# -*- coding: utf-8 -*-
#================================================================
#   God Bless You.
#
#   file name: split_text.py
#   author: klaus
#   email: klaus.cheng@qq.com
#   created date: 2019/05/08
#   description:
#
#================================================================

import os
import numpy as np

text_file = "./fn_text600W.txt"

each_part_n = 400000
save_dir = "./fn_text600W_split"

os.system("mkdir -p {}".format(save_dir))

with open(text_file, "r") as f:
    lines = f.read().splitlines()

n = len(lines)
np.random.shuffle(lines)
i = 0
cur = 0
while n > each_part_n:
    part_save_path = os.path.join(
        save_dir, text_file.replace(".txt", "_{}.txt".format(i)))
    with open(part_save_path, "w") as f:
        for line in lines[cur:cur + each_part_n]:
            f.write(line+ "\n")
    i += 1
    cur += each_part_n
    n -= each_part_n
