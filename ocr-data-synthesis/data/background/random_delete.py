# -*- coding: utf-8 -*-
#================================================================
#   God Bless You.
#
#   file name: random_delete.py
#   author: klaus
#   email: klaus.cheng@qq.com
#   created date: 2019/05/08
#   description:
#
#================================================================

import os
import glob
import numpy as np

pattern = "./ILSVRC2012_img_val_part/*.JPEG"
files = glob.glob(pattern)
np.random.shuffle(files)

LEAVE_N = 500
for i in range(LEAVE_N, len(files)):
    os.system("rm {}".format(files[i]))
