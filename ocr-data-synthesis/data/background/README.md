complete（完整背景，使用时切割部分）:
00.jpg：身份证背面
01.jpg：身份证正面

piece（碎片背景，直接使用即可）:
000.jpg-010.jpg：社保卡
011.jpg-019.jpg：医保卡
020.jpg-029.jpg：银行卡
030.jpg-033.jpg：驾驶证
034.jpg-040.jpg：火车票
041.jpg-049.jpg：营业执照
050.jpg-059.jpg：保险单据
060.jpg-066.jpg：身份证
067.jpg-073.jpg：护照
074.jpg-084.jpg：发票
085.jpg-090.jpg：回执单
091.jpg-096.jpg：医院单据
097.jpg-099.jpg：汇款申请书
100.jpg-323.jpg：银行单据

400.jpg-406.jpg：销售单
407.jpg-410.jpg：机动车注册大本
