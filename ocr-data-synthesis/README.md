# ocr-data-synthesis

准备通用印刷体的训练数据

## 1. Dependencies

- Pillow
- Opencv
- colormath
- imgaug

Install structions:
```
pip install opencv-contrib-python==3.4.2.17
pip install colormath==3.0.0
pip install imgaug==0.2.9
```

**Before running any scripts, add `data_synthesis` to PYTHONPATH by**:

```
source prepare_env.sh
```

## 2. Code Structure

```
|-- charset         charsets appeared in the label files
|-- old version     old data synthesis codes
|-- scripts         scripts for data generating
|-- data_synthesis  core synthesis code
    |-- utils           utils such as label replacement
    |-- generator.py    ocr image generate 
    |-- templates.py    parameter templates which controll the synthesis
    |-- text_generator.py   Generators for Fonts, Colors, Backgrounds, Texts
    |-- image_writer.py     Write text on images.
    |-- augmenter.py        Augmentation on text-image and background-image.
```

## 3. Usage

### 3.1 To script folder.

```
cd scripts
```

### 3.2 gen img list.

```
python gen_imglist.py
```
The generated file is `./data/texts/english_words_50w.txt`

examples:
<pre>
english_text_0000000.jpg anyone.
english_text_0000001.jpg Moore
english_text_0000002.jpg to this. 
english_text_0000003.jpg the
english_text_0000004.jpg urlLink
english_text_0000005.jpg  How can
english_text_0000006.jpg no where
english_text_0000007.jpg for
english_text_0000008.jpg u
english_text_0000009.jpg was
english_text_0000010.jpg   now
english_text_0000011.jpg (FFX
english_text_0000012.jpg list 5.
english_text_0000013.jpg even
</pre>

### 3.3 generate image.

```
python gen_img.py
```

### 3.4 generate label. 

```
python gen_label.py
```

Paths need to be modifed in the beyond processes.


## 4. Code Logic

call chain:
`scripts/gen_img.py` -> `data_synthesis/generator.py` -> `data_synthesis/image_writer.py`
